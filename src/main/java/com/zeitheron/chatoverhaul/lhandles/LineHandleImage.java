package com.zeitheron.chatoverhaul.lhandles;

import com.zeitheron.chatoverhaul.api.LineHandle;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineFile;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineImage;
import com.zeitheron.chatoverhaul.utils.DataFileHostAPI.DataFileHostEntity;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LineHandleImage extends LineHandle
{
	{
		setRegistryName("chatoverhaul", "image");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ChatLine toLine(NBTTagCompound nbt, int updateCounter)
	{
		ChatLineImage clf = new ChatLineImage(updateCounter, nbt.getInteger("ID"), nbt.getByteArray("Entity"));
		clf.deserializeNBT(nbt);
		return clf;
	}
}