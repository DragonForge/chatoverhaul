package com.zeitheron.chatoverhaul.lhandles;

import com.zeitheron.chatoverhaul.api.LineHandle;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineNew;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LineHandleText extends LineHandle
{
	{
		setRegistryName("chatoverhaul", "text");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ChatLine toLine(NBTTagCompound nbt, int updateCounter)
	{
		ChatLineNew cl = new ChatLineNew(updateCounter, ITextComponent.Serializer.fromJsonLenient(nbt.getString("Text")), nbt.getInteger("ID"));
		cl.deserializeNBT(nbt);
		return cl;
	}
}