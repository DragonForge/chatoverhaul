package com.zeitheron.chatoverhaul.lhandles;

import com.zeitheron.chatoverhaul.api.LineHandle;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineFile;
import com.zeitheron.chatoverhaul.utils.DataFileHostAPI.DataFileHostEntity;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LineHandleFile extends LineHandle
{
	{
		setRegistryName("chatoverhaul", "file");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ChatLine toLine(NBTTagCompound nbt, int updateCounter)
	{
		ChatLineFile clf = new ChatLineFile(updateCounter, nbt.getInteger("ID"), DataFileHostEntity.deserialize(nbt.getCompoundTag("Entity")));
		clf.deserializeNBT(nbt);
		return clf;
	}
}