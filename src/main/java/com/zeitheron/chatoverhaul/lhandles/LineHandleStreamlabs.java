package com.zeitheron.chatoverhaul.lhandles;

import com.zeitheron.chatoverhaul.api.LineHandle;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineImage;
import com.zeitheron.chatoverhaul.lhandles.lines.streamlabs.ChatLineDonate;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LineHandleStreamlabs extends LineHandle
{
	{
		setRegistryName("chatoverhaul", "streamlabs");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ChatLine toLine(NBTTagCompound nbt, int updateCounter)
	{
		String type = nbt.getString("type");
		if(type.equals("donation"))
		{
			ChatLineDonate d = new ChatLineDonate(updateCounter, nbt.getInteger("ID"), nbt);
		}
		
		return null;
	}
}