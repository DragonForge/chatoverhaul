package com.zeitheron.chatoverhaul.lhandles.lines;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

import javax.imageio.ImageIO;

import com.google.common.util.concurrent.AtomicDouble;
import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.client.gui.GuiOverhaulChat;
import com.zeitheron.chatoverhaul.utils.ImageCompressor;
import com.zeitheron.chatoverhaul.utils.DataFileHostAPI.DataFileHostEntity;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.texture.TexLocUploader;
import com.zeitheron.hammercore.lib.zlib.audio.AudioSampleHelper;
import com.zeitheron.hammercore.utils.FileSizeMetric;
import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.event.ClickEvent;

public class ChatLineVoice extends ChatLineNew
{
	public static final int MAX_RMS_POINTS = 100;
	
	public byte[] entity;
	public float[] volumes;
	public String sender;
	
	public AtomicDouble progress = new AtomicDouble();
	public double playProgress, prevPlayProgress;
	
	public boolean canPlay = true;
	
	public final UUID uuid = UUID.randomUUID();
	
	public static ITextComponent file()
	{
		return new TextComponentTranslation("chatoverhaul.voice");
	}
	
	public ChatLineVoice(int updateCounterCreatedIn, int chatLineIDIn)
	{
		super(updateCounterCreatedIn, file(), chatLineIDIn);
	}
	
	public ChatLineVoice(int updateCounterCreatedIn, int chatLineIDIn, byte[] ent)
	{
		super(updateCounterCreatedIn, file(), chatLineIDIn);
		setEntity(ent);
	}
	
	@Override
	public void update()
	{
		prevPlayProgress = playProgress;
		playProgress = progress.get();
	}
	
	public void setEntity(byte[] entity)
	{
		this.entity = entity;
		
		volumes = new float[MAX_RMS_POINTS];
		byte[] buf = new byte[entity.length / MAX_RMS_POINTS];
		
		int j = 0;
		for(int i = 0; i < entity.length; i += buf.length)
		{
			System.arraycopy(entity, i, buf, 0, buf.length);
			volumes[j] = AudioSampleHelper.getRMS(AudioSampleHelper.asSamples(buf, buf.length));
			++j;
		}
	}
	
	public byte[] getEntity()
	{
		return entity;
	}
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = super.serializeNBT();
		nbt.setByteArray("Entity", entity);
		return nbt;
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		super.deserializeNBT(nbt);
		setEntity(nbt.getByteArray("Entity"));
		sender = nbt.getString("Sender");
		if(getChatComponent() == null)
			try
			{
				FinalFieldHelper.setFinalField(ChatLine.class.getDeclaredFields()[1], this, file());
			} catch(SecurityException | ReflectiveOperationException e)
			{
				e.printStackTrace();
			}
	}
	
	@Override
	public int getWidth()
	{
		return 2 + getFont().getStringWidth("<" + sender + "> ") + 24;
	}
	
	@Override
	public int getHeight()
	{
		return 24;
	}
	
	@Override
	public void handleMouseClick(int x, int y)
	{
		if(entity != null)
		{
			GuiScreen gs = Minecraft.getMinecraft().currentScreen;
			
			if(gs != null)
			{
				int pw = 4 + 22;
				int oix = 2 + getFont().getStringWidth("<" + sender + "> ");
				ITextComponent component = new TextComponentString("<Voice>");
				if(x >= oix)
				{
					if(x < oix + pw)
					{
						component.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chatoverhaul_play_voice " + uuid.toString()));
					}
				} else
					component.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chatoverhaul_open_profile " + sender));
				gs.handleComponentClick(component);
			}
		}
	}
	
	@Override
	public boolean render(int alpha255, int x, int y)
	{
		boolean hov = GuiOverhaulChat.selectedLine == this;
		
		if(alpha255 > 3 && entity != null)
		{
			int i2 = 0;
			
			int a = alpha255 << 24;
			
			Gui.drawRect(0, y - getHeight() + 9, x + 4, y + 9, ((int) (alpha255 / (hov ? 1.4F : 2))) << 24);
			GlStateManager.enableBlend();
			String s = "<" + sender + "> ";
			getFont().drawStringWithShadow(s, 2F, (float) (y + 1 - 7.5F), 16777215 | a);
			
			int sw = getFont().getStringWidth(s);
			boolean hl = hov && GuiOverhaulChat.mouseX >= 2 + sw;
			
			ChatOverhaul.proxy.bindTexture("voice");
			GlStateManager.pushMatrix();
			GlStateManager.color(1, 1, 1, alpha255 / 255F);
			GlStateManager.translate(sw, y - getHeight() + 10 + (getHeight() - 22) / 2, 0);
			RenderUtil.drawFullTexturedModalRect(0, 0, 22, 22);
			GlStateManager.popMatrix();
			
			double val = prevPlayProgress + (playProgress - prevPlayProgress) * Minecraft.getMinecraft().getRenderPartialTicks();
			
			for(int i = 0; i < volumes.length; ++i)
			{
				float vol = Math.min(1, Math.max(0, volumes[i]) * 6F);
				vol = vol * vol;
				
				boolean upto = val > i / (float) volumes.length;
				
				Gui.drawRect(sw + 24 + i, y - getHeight() + 9 + Math.round(21 - 21 * vol) + (getHeight() - 22) / 2, sw + 25 + i, y - getHeight() + 10 + (getHeight() - 22) / 2 + 21, (upto ? 0x66EEFF : 0xFFFFFF) | a);
			}
			
			GlStateManager.disableAlpha();
			GlStateManager.disableBlend();
		}
		
		return hov;
	}
}