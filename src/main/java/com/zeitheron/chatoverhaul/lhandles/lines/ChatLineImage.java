package com.zeitheron.chatoverhaul.lhandles.lines;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

import javax.imageio.ImageIO;

import com.zeitheron.chatoverhaul.client.gui.GuiOverhaulChat;
import com.zeitheron.chatoverhaul.utils.ImageCompressor;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.texture.TexLocUploader;
import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.event.ClickEvent;

public class ChatLineImage extends ChatLineNew
{
	public byte[] entity;
	public String sender;
	
	public final UUID uuid = UUID.randomUUID();
	
	public BufferedImage preview, full;
	
	public static ITextComponent file()
	{
		return new TextComponentTranslation("chatoverhaul.image");
	}
	
	public ChatLineImage(int updateCounterCreatedIn, int chatLineIDIn)
	{
		super(updateCounterCreatedIn, file(), chatLineIDIn);
	}
	
	public ChatLineImage(int updateCounterCreatedIn, int chatLineIDIn, byte[] ent)
	{
		super(updateCounterCreatedIn, file(), chatLineIDIn);
		setEntity(ent);
	}
	
	public void setEntity(byte[] entity)
	{
		this.entity = entity;
		
		try
		{
			full = ImageIO.read(new ByteArrayInputStream(entity));
			if(full != null)
				preview = ImageCompressor.scaleDownTo(300, 23, full);
		} catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public byte[] getEntity()
	{
		return entity;
	}
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = super.serializeNBT();
		nbt.setByteArray("Entity", entity);
		return nbt;
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		super.deserializeNBT(nbt);
		setEntity(nbt.getByteArray("Entity"));
		sender = nbt.getString("Sender");
		if(getChatComponent() == null)
			try
			{
				FinalFieldHelper.setFinalField(ChatLine.class.getDeclaredFields()[1], this, file());
			} catch(SecurityException | ReflectiveOperationException e)
			{
				e.printStackTrace();
			}
	}
	
	@Override
	public int getWidth()
	{
		return 2 + getFont().getStringWidth("<" + sender + "> ") + 2 + (preview != null ? preview.getWidth() : 0);
	}
	
	@Override
	public int getHeight()
	{
		return 24;
	}
	
	@Override
	public void handleMouseClick(int x, int y)
	{
		if(entity != null)
		{
			GuiScreen gs = Minecraft.getMinecraft().currentScreen;
			
			if(gs != null)
			{
				int pw = 4 + (preview != null ? preview.getWidth() : 0);
				int oix = 2 + getFont().getStringWidth("<" + sender + "> ");
				ITextComponent component = new TextComponentString("<Image: " + preview.getWidth() + "x" + preview.getHeight() + ">");
				if(x >= oix)
				{
					if(x < oix + pw)
					{
						component.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chatoverhaul_open_image " + uuid.toString()));
						Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
					}
				} else
					component.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chatoverhaul_open_profile " + sender));
				gs.handleComponentClick(component);
			}
		}
	}
	
	ResourceLocation previewTex, fullTex;
	
	public ResourceLocation getFullTex()
	{
		return fullTex;
	}
	
	public ResourceLocation getPreviewTex()
	{
		return previewTex;
	}
	
	@Override
	public boolean render(int alpha255, int x, int y)
	{
		boolean hov = GuiOverhaulChat.selectedLine == this;
		
		if(alpha255 > 3 && entity != null)
		{
			int i2 = 0;
			
			int a = alpha255 << 24;
			
			Gui.drawRect(0, y - getHeight() + 9, x + 4, y + 9, ((int) (alpha255 / (hov ? 1.4F : 2))) << 24);
			GlStateManager.enableBlend();
			String s = "<" + sender + "> ";
			getFont().drawStringWithShadow(s, 2F, (float) (y + 1 - 7.5F), 16777215 | a);
			
			int sw = getFont().getStringWidth(s);
			
			if(previewTex == null && preview != null)
			{
				previewTex = new ResourceLocation("chatoverhaul", "textures/chat_messages/" + uuid + ".preview");
				TexLocUploader.upload(previewTex, preview);
				TexLocUploader.cleanupAfterLogoff(previewTex);
			}
			
			if(fullTex == null && full != null)
			{
				fullTex = new ResourceLocation("chatoverhaul", "textures/chat_messages/" + uuid + ".full");
				
				// Make smaller full image limits so we don't clog our VRAM so
				// much
				TexLocUploader.upload(fullTex, ImageCompressor.scaleDownTo(4096, 4096, full));
				
				TexLocUploader.cleanupAfterLogoff(fullTex);
			}
			
			if(preview != null && fullTex != null)
			{
				Minecraft.getMinecraft().getTextureManager().bindTexture(fullTex);
				GlStateManager.pushMatrix();
				GlStateManager.translate(sw + 4, y - getHeight() + 9.5F + (getHeight() - preview.getHeight()) / 2, 0);
				RenderUtil.drawFullTexturedModalRect(0, 0, preview.getWidth(), preview.getHeight());
				GlStateManager.popMatrix();
			}
			
			GlStateManager.disableAlpha();
			GlStateManager.disableBlend();
		}
		
		return hov;
	}
}