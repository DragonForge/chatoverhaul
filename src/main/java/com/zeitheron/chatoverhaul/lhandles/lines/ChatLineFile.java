package com.zeitheron.chatoverhaul.lhandles.lines;

import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.client.gui.GuiOverhaulChat;
import com.zeitheron.chatoverhaul.utils.DataFileHostAPI.DataFileHostEntity;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.utils.FileSizeMetric;
import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.event.ClickEvent;

public class ChatLineFile extends ChatLineNew
{
	public DataFileHostEntity entity;
	public String sender;
	
	public static ITextComponent file()
	{
		return new TextComponentTranslation("chatoverhaul.file");
	}
	
	public ChatLineFile(int updateCounterCreatedIn, int chatLineIDIn)
	{
		super(updateCounterCreatedIn, file(), chatLineIDIn);
	}
	
	public ChatLineFile(int updateCounterCreatedIn, int chatLineIDIn, DataFileHostEntity ent)
	{
		super(updateCounterCreatedIn, file(), chatLineIDIn);
		setEntity(ent);
	}
	
	public void setEntity(DataFileHostEntity entity)
	{
		this.entity = entity;
	}
	
	public DataFileHostEntity getEntity()
	{
		return entity;
	}
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = super.serializeNBT();
		nbt.setTag("Entity", entity.serialize());
		return nbt;
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		super.deserializeNBT(nbt);
		entity = DataFileHostEntity.deserialize(nbt.getCompoundTag("Entity"));
		sender = nbt.getString("Sender");
		if(getChatComponent() == null)
			try
			{
				FinalFieldHelper.setFinalField(ChatLine.class.getDeclaredFields()[1], this, file());
			} catch(SecurityException | ReflectiveOperationException e)
			{
				e.printStackTrace();
			}
	}
	
	@Override
	public int getWidth()
	{
		return 2 + getFont().getStringWidth("<" + sender + "> ") + 20 + getFont().getStringWidth(entity.filename + " | " + FileSizeMetric.toMaxSize(entity.size));
	}
	
	@Override
	public int getHeight()
	{
		return 24;
	}
	
	@Override
	public void handleMouseClick(int x, int y)
	{
		if(entity != null)
		{
			GuiScreen gs = Minecraft.getMinecraft().currentScreen;
			
			if(gs != null)
			{
				ITextComponent component = new TextComponentString("<File: " + entity.filename + ">");
				if(x >= 2 + getFont().getStringWidth("<" + sender + "> "))
				{
					component.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, entity.url));
					Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
				} else
					component.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chatoverhaul_open_profile " + sender));
				gs.handleComponentClick(component);
			}
		}
	}
	
	@Override
	public boolean render(int alpha255, int x, int y)
	{
		boolean hov = GuiOverhaulChat.selectedLine == this;
		
		if(alpha255 > 3 && entity != null)
		{
			int i2 = 0;
			
			int a = alpha255 << 24;
			
			Gui.drawRect(0, y - getHeight() + 9, x + 4, y + 9, ((int) (alpha255 / (hov ? 1.4F : 2))) << 24);
			GlStateManager.enableBlend();
			String s = "<" + sender + "> ";
			getFont().drawStringWithShadow(s, 2F, (float) (y + 1 - 7.5F), 16777215 | a);
			ChatOverhaul.proxy.bindTexture("file");
			
			boolean hl = hov && GuiOverhaulChat.mouseX >= 2 + getFont().getStringWidth("<" + sender + "> ");
			
			int xo = 2 + getFont().getStringWidth(s);
			GlStateManager.color(1F, 1F, hl ? .4F : 1F, alpha255 / 255F);
			
			RenderUtil.drawFullTexturedModalRect(xo, y - getHeight() + 11, 20, 20);
			xo += 20;
			
			s = entity.filename + " | " + FileSizeMetric.toMaxSize(entity.size);
			getFont().drawStringWithShadow(s, xo, (float) (y + 1 - 7.5F), (hl ? 0xFFFF66 : 0xFFFFFF) | a);
			
			GlStateManager.disableAlpha();
			GlStateManager.disableBlend();
		}
		
		return hov;
	}
}