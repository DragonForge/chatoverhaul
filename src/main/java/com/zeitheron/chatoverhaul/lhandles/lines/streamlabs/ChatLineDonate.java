package com.zeitheron.chatoverhaul.lhandles.lines.streamlabs;

import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineNew;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class ChatLineDonate extends ChatLineNew
{
	public String sender, amt, desc;
	
	public static ITextComponent file()
	{
		return new TextComponentTranslation("chatoverhaul.file");
	}
	
	public ChatLineDonate(int updateCounterCreatedIn, int chatLineIDIn)
	{
		super(updateCounterCreatedIn, file(), chatLineIDIn);
	}
	
	public ChatLineDonate(int updateCounterCreatedIn, int chatLineIDIn, NBTTagCompound data)
	{
		super(updateCounterCreatedIn, file(), chatLineIDIn);
		setEntity(data);
	}
	
	public void setEntity(NBTTagCompound data)
	{
		sender = data.getString("name");
		amt = data.getString("formatted_amount");
		desc = data.getString("message");
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		super.deserializeNBT(nbt);
		sender = nbt.getString("Sender");
		if(getChatComponent() == null)
			try
			{
				FinalFieldHelper.setFinalField(ChatLine.class.getDeclaredFields()[1], this, file());
			} catch(SecurityException | ReflectiveOperationException e)
			{
				e.printStackTrace();
			}
	}
	
	@Override
	public int getWidth()
	{
		return 320;
	}
	
	@Override
	public int getHeight()
	{
		return 24 + getFont().getWordWrappedHeight(desc, 323);
	}
	
	@Override
	public boolean render(int alpha255, int x, int y)
	{
		if(alpha255 > 3)
		{
			int i2 = 0;
			
			int a = alpha255 << 24;
			
			Gui.drawRect(0, y - getHeight() + 9, x + 4, y + 9, alpha255 / 2 << 24);
			
			GlStateManager.enableAlpha();
			GlStateManager.enableBlend();
			
			ChatOverhaul.proxy.bindTexture("money");
			GlStateManager.color(1F, 1F, 1F, alpha255 / 255F);
			RenderUtil.drawFullTexturedModalRect(0, y - getHeight() + 9, 24, 24);
			
			getFont().drawString(sender + " (" + TextFormatting.GOLD + amt + TextFormatting.RESET + ")", 26, y - getHeight() + 17, 0x008800 | a);
			getFont().drawSplitString(desc, 2, y - getHeight() + 31, 323, 0xFFFFFF | a);
			
			Gui.drawRect(0, y - getHeight() + 10, 324, y - getHeight() + 9, 0xFF9900 | a);
			
			Gui.drawRect(323, y - getHeight() + 10, 324, y + 8, 0xFF9900 | a);
			Gui.drawRect(0, y - getHeight() + 10, 1, y + 8, 0xFF9900 | a);
			
			Gui.drawRect(0, y + 8, 324, y + 9, 0xFF9900 | a);
		}
		return false;
	}
}