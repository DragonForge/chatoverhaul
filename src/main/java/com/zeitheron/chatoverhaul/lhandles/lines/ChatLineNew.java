package com.zeitheron.chatoverhaul.lhandles.lines;

import java.util.UUID;

import com.zeitheron.chatoverhaul.api.IRenderableChatLine;
import com.zeitheron.chatoverhaul.client.gui.GuiOverhaulChat;
import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;
import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.ClickEvent.Action;
import net.minecraft.util.text.event.HoverEvent;

public class ChatLineNew extends ChatLine implements IRenderableChatLine
{
	FontRenderer font;
	
	public ChatLineNew(int updateCounterCreatedIn, ITextComponent lineStringIn, int chatLineIDIn)
	{
		super(updateCounterCreatedIn, lineStringIn, chatLineIDIn);
	}
	
	private ChatFingerprint print;
	
	@Override
	public ChatFingerprint getPrint()
	{
		return print;
	}
	
	@Override
	public void setPrint(ChatFingerprint print)
	{
		this.print = print;
	}
	
	public FontRenderer getFont()
	{
		if(font == null)
			font = Minecraft.getMinecraft().fontRenderer;
		return font;
	}
	
	public void setFont(FontRenderer font)
	{
		this.font = font;
	}
	
	@Override
	public int getWidth()
	{
		GuiNewChat gnc = Minecraft.getMinecraft().ingameGUI.getChatGUI();
		int k = MathHelper.ceil((float) gnc.getChatWidth() / gnc.getChatScale());
		return Math.min(k, getFont().getStringWidth(getChatComponent().getFormattedText()));
	}
	
	@Override
	public int getHeight()
	{
		GuiNewChat gnc = Minecraft.getMinecraft().ingameGUI.getChatGUI();
		int k = MathHelper.ceil((float) gnc.getChatWidth() / gnc.getChatScale());
		return getFont().getWordWrappedHeight(getChatComponent().getFormattedText(), k);
	}
	
	@Override
	public ITextComponent transformHoveredComponent(ITextComponent component)
	{
		String text = component.getUnformattedComponentText();
		ClickEvent ce = component.getStyle().getClickEvent();
		
		// For some odd reasons, getUnformattedComponentText does not remove
		// color codes, as described by Forge. So we do it manually. Oh well...
		for(TextFormatting tf : TextFormatting.values())
			text = text.replaceAll(tf.toString(), "");
		
		if(ce != null && ce.getAction() == Action.SUGGEST_COMMAND && ce.getValue().toLowerCase().contains("/msg " + text.toLowerCase()))
		{
			Style st = component.getStyle().createDeepCopy();
			st.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chatoverhaul_open_profile " + text));
			st.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentTranslation("chatoverhaul.viewprofile")));
			component.setStyle(st);
		}
		
		return component;
	}
	
	@Override
	public boolean render(int alpha255, int x, int y)
	{
		boolean hov = GuiOverhaulChat.selectedLine == this;
		
		if(alpha255 > 3)
		{
			int i2 = 0;
			Gui.drawRect(0, y + 9 - getHeight(), x + 4, y + 9, ((int) (alpha255 / (hov ? 1.4F : 2))) << 24);
			GlStateManager.enableBlend();
			String ncc = getChatComponent().getUnformattedText();
			for(TextFormatting tf : TextFormatting.values())
				if(!tf.isFancyStyling())
					ncc = ncc.replaceAll(tf.toString(), "");
			getFont().drawSplitString(ncc, 3, y + 11 - getHeight(), x, alpha255 << 24);
			getFont().drawSplitString(getChatComponent().getFormattedText(), 2, y + 10 - getHeight(), x, 0xFFFFFF + (alpha255 << 24));
			GlStateManager.disableAlpha();
			GlStateManager.disableBlend();
		}
		
		return hov;
	}
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setString("Text", ITextComponent.Serializer.componentToJson(getChatComponent()));
		if(getChatLineID() != 0)
			nbt.setInteger("ID", getChatLineID());
		return nbt;
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		try
		{
			FinalFieldHelper.setFinalField(ChatLine.class.getDeclaredFields()[1], this, ITextComponent.Serializer.fromJsonLenient(nbt.getString("Text")));
			FinalFieldHelper.setFinalField(ChatLine.class.getDeclaredFields()[0], this, nbt.getInteger("ID"));
		} catch(SecurityException | ReflectiveOperationException e)
		{
			e.printStackTrace();
		}
	}
}