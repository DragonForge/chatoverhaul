package com.zeitheron.chatoverhaul.lhandles;

import com.zeitheron.chatoverhaul.api.LineHandle;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineVoice;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LineHandleVoice extends LineHandle
{
	{
		setRegistryName("chatoverhaul", "voice");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ChatLine toLine(NBTTagCompound nbt, int updateCounter)
	{
		ChatLineVoice clf = new ChatLineVoice(updateCounter, nbt.getInteger("ID"), nbt.getByteArray("Entity"));
		clf.deserializeNBT(nbt);
		return clf;
	}
}