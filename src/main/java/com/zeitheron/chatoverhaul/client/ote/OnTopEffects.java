package com.zeitheron.chatoverhaul.client.ote;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.chatoverhaul.client.utils.UserTypingData;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class OnTopEffects
{
	public static List<OTEffect> effects = new ArrayList<>();
	
	@SubscribeEvent
	public void renderScreen(RenderGameOverlayEvent e)
	{
		if(!(e instanceof RenderGameOverlayEvent.Post))
			return;
		if(e.getType() != RenderGameOverlayEvent.ElementType.ALL)
			return;
		
		Minecraft mc = Minecraft.getMinecraft();
		
		if(resolution != null)
			mc.fontRenderer.drawString(UserTypingData.getTypingString(), 2, resolution.getScaledHeight() - 38, 0xFFFFFFFF, true);
		
		float pt = Minecraft.getMinecraft().getRenderPartialTicks();
		
		for(int i = 0; i < effects.size(); ++i)
		{
			OTEffect eff = effects.get(i);
			
			if(eff.expired || !eff.renderHud)
				continue;
			
			eff.currentGui = null;
			
			eff.render(pt);
		}
	}
	
	@SubscribeEvent
	public void renderOnTop(GuiScreenEvent.DrawScreenEvent.Post e)
	{
		GuiScreen gs = e.getGui();
		int mx = e.getMouseX(), my = e.getMouseY();
		float pt = Minecraft.getMinecraft().getRenderPartialTicks();
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(0, 0, 300);
		for(int i = 0; i < effects.size(); ++i)
		{
			OTEffect eff = effects.get(i);
			
			eff.mouseX = mx;
			eff.mouseY = my;
			eff.currentGui = gs;
			
			if(eff.expired || !eff.renderGui)
				continue;
			
			eff.render(pt);
		}
		GlStateManager.popMatrix();
	}
	
	public ScaledResolution resolution;
	
	@SubscribeEvent
	public void tick(ClientTickEvent e)
	{
		if(e.phase == Phase.START)
		{
			for(int i = 0; i < effects.size(); ++i)
			{
				OTEffect eff = effects.get(i);
				
				if(eff.expired)
				{
					effects.remove(i);
					continue;
				}
				
				eff.update();
			}
			
			ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft());
			
			if(resolution == null)
				resolution = sr;
			
			if(sr.getScaledHeight() != resolution.getScaledHeight() || sr.getScaledWidth() != resolution.getScaledWidth())
			{
				for(int i = 0; i < effects.size(); ++i)
				{
					OTEffect eff = effects.get(i);
					eff.resize(resolution, sr);
				}
			}
			
			resolution = sr;
		}
	}
}