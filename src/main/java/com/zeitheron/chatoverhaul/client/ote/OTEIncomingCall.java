package com.zeitheron.chatoverhaul.client.ote;

import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.api.IChatInteractiveWidget;
import com.zeitheron.chatoverhaul.client.gui.GuiChatCO;
import com.zeitheron.chatoverhaul.client.utils.RenderPlayerBody;
import com.zeitheron.chatoverhaul.client.utils.RenderPlayerBody.PlayerRenderingInfo;
import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.chatoverhaul.net.vc.PacketAcceptVC;
import com.zeitheron.chatoverhaul.net.vc.PacketDenyVC;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.net.HCNet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;

public class OTEIncomingCall extends OTEffect implements IChatInteractiveWidget
{
	public float alpha, prevAlpha;
	
	public static OTEIncomingCall instance;
	
	public int ticksActive = 0;
	
	public static void show()
	{
		if(instance == null)
		{
			instance = new OTEIncomingCall();
			OnTopEffects.effects.add(instance);
		} else if(!OnTopEffects.effects.contains(instance))
		{
			instance.expired = false;
			instance.ticksActive = 0;
			OnTopEffects.effects.add(instance);
		}
	}
	
	public final PlayerRenderingInfo fakePlayerInfo = new PlayerRenderingInfo();
	
	DynGuiTex tex;
	
	@Override
	public void update()
	{
		super.update();
		
		expired = VoiceChatReceiver.INCOMING == null;
		
		if(!expired)
		{
			NetworkPlayerInfo npi = Minecraft.getMinecraft().getConnection().getPlayerInfo(VoiceChatReceiver.INCOMING);
			if(npi != null)
				fakePlayerInfo.profile = npi.getGameProfile();
			++ticksActive;
		} else
			return;
		
		xSize = 96;
		ySize = 36;
		
		x = (width - xSize) / 2;
		y = height / 2 - ySize * 1.5;
		
		renderGui = true;
		renderHud = true;
		
		// Deny VC
		if(ticksActive >= 30 * 20 && VoiceChatReceiver.INCOMING != null)
		{
			HCNet.INSTANCE.sendToServer(new PacketDenyVC().setTarget(VoiceChatReceiver.INCOMING));
			VoiceChatReceiver.INCOMING = null;
		}
	}
	
	@Override
	public void render(float partialTime)
	{
		int mx, my;
		
		if(currentGui instanceof GuiChatCO)
		{
			mx = (int) x + 48 - mouseX;
			my = (int) y + 30 - mouseY - 25;
		} else
		{
			if(currentGui != null)
				return;
			mx = 0;
			my = 0;
		}
		
		if(tex == null)
		{
			GuiTexBakery b = new GuiTexBakery();
			b.body(0, 0, 96, 36);
			tex = b.bake();
		}
		
		tex.theme = GuiTheme.current();
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, 0);
		
		tex.render(0, 0);
		
		GlStateManager.enableBlend();
		
		boolean hov = mouseX >= x + 6 && mouseY >= y + 6 && mouseX < x + 6 + 24 && mouseY < y + 30;
		
		GlStateManager.color(hov ? .25F : 1F, 1F, hov ? .25F : 1F, 1F);
		
		ChatOverhaul.proxy.bindTexture("accept_call");
		RenderUtil.drawFullTexturedModalRect(6, 6, 24, 24);
		
		hov = mouseX >= x + 64 && mouseY >= y + 6 && mouseX < x + 64 + 24 && mouseY < y + 30;
		
		GlStateManager.color(1F, hov ? .5F : 1F, hov ? .5F : 1F, 1F);
		ChatOverhaul.proxy.bindTexture("close_call");
		RenderUtil.drawFullTexturedModalRect(64, 6, 24, 24);
		
		GlStateManager.popMatrix();
		
		GlStateManager.color(1F, 1F, 1F, 1F);
		GlStateManager.pushMatrix();
		
		if(fakePlayerInfo.profile != null)
			RenderPlayerBody.drawEntityOnScreen((int) x + 48, (int) y + 30, 12F, mx, my, fakePlayerInfo);
		
		int ticks = Minecraft.getMinecraft().player.ticksExisted % 40;
		
		for(int i = 0; i < 2; ++i)
		{
			int time = Math.max(0, Math.min(10, ticks - i * (10 + i * 2)));
			
			float val = 1F + time / 60F;
			float alpha = 1F - time / 10F;
			
			GlStateManager.pushMatrix();
			
			GlStateManager.color(1F, 1F, 1F, alpha);
			
			GlStateManager.translate(x + 48, y + 30, 0);
			
			GlStateManager.translate(0, -12, 0);
			GlStateManager.scale(val, val, val);
			GlStateManager.translate(0, 12, 0);
			
			RenderPlayerBody.drawEntityOnScreen(0, 0, 12F, mx, my, fakePlayerInfo);
			GlStateManager.popMatrix();
		}
		
		GlStateManager.color(1F, 1F, 1F, 1F);
		GlStateManager.popMatrix();
	}
	
	@Override
	public void mouseClick(int relX, int relY, int mouseButton)
	{
		if(mouseButton == 0 && relX >= 64 && relY >= 6 && relX < 64 + 24 && relY < 30)
		{
			HCNet.INSTANCE.sendToServer(new PacketDenyVC().setMode(false).setTarget(VoiceChatReceiver.INCOMING));
			VoiceChatReceiver.INCOMING = null;
			
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
		}
		
		if(mouseButton == 0 && relX >= 6 && relY >= 6 && relX < 30 && relY < 30)
		{
			HCNet.INSTANCE.sendToServer(new PacketAcceptVC().setTarget(VoiceChatReceiver.INCOMING));
			VoiceChatReceiver.INCOMING = null;
			
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
		}
	}
}