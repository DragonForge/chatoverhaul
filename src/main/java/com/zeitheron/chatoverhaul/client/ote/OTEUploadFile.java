package com.zeitheron.chatoverhaul.client.ote;

import java.io.File;

import org.lwjgl.opengl.GL11;

import com.google.common.util.concurrent.AtomicDouble;
import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.hammercore.client.utils.RenderUtil;

import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;

public class OTEUploadFile extends OTEffect
{
	private double tx, ty, ox, oy;
	public AtomicDouble progress;
	public File file;
	
	public OTEUploadFile(double x, double y, double tx, double ty, AtomicDouble progress, File file)
	{
		renderGui = false;
		renderHud = true;
		
		this.file = file;
		this.progress = progress;
		this.x = this.prevX = this.ox = x;
		this.y = this.prevY = this.oy = y;
		this.tx = tx;
		this.ty = ty;
	}
	
	@Override
	public void resize(ScaledResolution prev, ScaledResolution nev)
	{
		super.resize(prev, nev);
		tx = handleResizeXd(tx, prev, nev);
		ty = handleResizeYd(ty, prev, nev);
	}
	
	int timer;
	
	@Override
	public void update()
	{
		super.update();
		
		++timer;
		
		if(timer < 120)
		{
			float p = timer / 120F;
			
			x = ox + (tx - ox) * p;
			y = oy + (ty - oy) * p;
		}
		
		if(this.progress.get() > 1.)
			setExpired();
	}
	
	@Override
	public void render(float partialTime)
	{
		double cx = prevX + (x - prevX) * partialTime;
		double cy = prevY + (y - prevY) * partialTime;
		
		GlStateManager.enableAlpha();
		RenderHelper.disableStandardItemLighting();
		
		float scale = 1F;
		
		GlStateManager.enableBlend();
		
		GL11.glPushMatrix();
		GL11.glColor4f(.75F, .75F, .75F, 1);
		
		GL11.glTranslated(cx - 16 * scale / 2, cy - 16 * scale / 2, 0);
		GL11.glScaled(scale, scale, scale);
		
		{
			ChatOverhaul.proxy.bindTexture("upload_file");
			RenderUtil.drawFullTexturedModalRect(0, 0, 16, 16);
			
			GlStateManager.translate(1, 5, 0);
			GlStateManager.scale(1 / 16D, 1 / 16D, 1 / 16D);
			GL11.glColor4f(.25F, .75F, .25F, 1);
			
			double progress = 105 * Math.min(1, this.progress.get());
			
			double p1 = Math.min(94, progress) / 94F;
			double p2 = Math.max(0, progress - 94) / 11F;
			
			RenderUtil.drawTexturedModalRect(5, 2 + 94 - 94 * p1, 21, 82 + 94 - 94 * p1, 118, 94 * p1);
			RenderUtil.drawTexturedModalRect(55, -9 + 11 - 11 * p2, 71, 71 + 11 - 11 * p2, 32, 11 * p2);
		}
		GL11.glColor4f(1, 1, 1, 1);
		GL11.glPopMatrix();
	}
}