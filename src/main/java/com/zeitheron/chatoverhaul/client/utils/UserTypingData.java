package com.zeitheron.chatoverhaul.client.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.mojang.authlib.GameProfile;
import com.zeitheron.hammercore.lib.zlib.utils.Joiner;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.resources.I18n;

public class UserTypingData
{
	public static final List<GameProfile> TYPING_PROFILES = new ArrayList<>();
	public static final List<UUID> TYPING_UUIDS = new ArrayList<>();
	public static final IntList TYPING_TIMERS = new IntArrayList();
	
	public static void setTyping(UUID user)
	{
		if(TYPING_UUIDS.contains(user))
		{
			TYPING_TIMERS.set(TYPING_UUIDS.indexOf(user), 0);
			return;
		}
		
		NetHandlerPlayClient nhpc = Minecraft.getMinecraft().getConnection();
		if(nhpc != null)
		{
			NetworkPlayerInfo npi = nhpc.getPlayerInfo(user);
			if(npi != null)
			{
				TYPING_UUIDS.add(user);
				TYPING_PROFILES.add(npi.getGameProfile());
				TYPING_TIMERS.add(0);
			}
		}
	}
	
	public static void update()
	{
		for(int i = 0; i < TYPING_TIMERS.size(); ++i)
		{
			int val;
			TYPING_TIMERS.set(i, val = TYPING_TIMERS.get(i) + 1);
			if(val >= 100)
			{
				TYPING_UUIDS.remove(i);
				TYPING_PROFILES.remove(i);
				TYPING_TIMERS.remove(i);
			}
		}
	}
	
	public static String getTypingString()
	{
		if(TYPING_UUIDS.isEmpty())
			return "";
		if(TYPING_UUIDS.size() >= 5)
			return I18n.format("chatoverhaul.fewtyping");
		List<String> vals = new ArrayList<>();
		for(int i = 0; i < TYPING_UUIDS.size(); ++i)
		{
//			UUID ui = TYPING_UUIDS.get(i);
//			if(ui != null && !ui.equals(Minecraft.getMinecraft().player.getGameProfile().getId()))
				vals.add(TYPING_PROFILES.get(i).getName());
		}
		if(vals.isEmpty())
			return "";
		String last = vals.remove(vals.size() - 1);
		if(vals.isEmpty())
			return I18n.format("chatoverhaul.istyping", last);
		else
			return I18n.format("chatoverhaul.aretyping", Joiner.on(" ").join(vals), last);
	}
}