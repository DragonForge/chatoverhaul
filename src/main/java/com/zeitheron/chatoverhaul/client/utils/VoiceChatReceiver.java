package com.zeitheron.chatoverhaul.client.utils;

import java.awt.AWTException;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

import com.zeitheron.chatoverhaul.lib.iflow.FlowImageGL;
import com.zeitheron.chatoverhaul.lib.iflow.ImageCompressor;
import com.zeitheron.chatoverhaul.lib.iflow.ScreenCapture;
import com.zeitheron.chatoverhaul.net.NetTransportSendScreen;
import com.zeitheron.chatoverhaul.net.NetTransportSendScreenMP;
import com.zeitheron.chatoverhaul.net.vc.PacketSetupScreenShare;
import com.zeitheron.chatoverhaul.net.vc.PacketStopVC;
import com.zeitheron.chatoverhaul.net.vc.PacketTransferBytesVC;
import com.zeitheron.chatoverhaul.net.vc.PacketTransportVideo;
import com.zeitheron.chatoverhaul.utils.BlobbedOutputStream;
import com.zeitheron.chatoverhaul.utils.CachingInputStream;
import com.zeitheron.hammercore.lib.zlib.audio.AudioSampleHelper;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.transport.TransportSessionBuilder;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatList;
import net.minecraftforge.fml.common.thread.SidedThreadGroups;

public class VoiceChatReceiver
{
	public static UUID INCOMING, OUTGOING;
	
	public static VoiceChatReceiver VC;
	
	public final AudioFormat format = new AudioFormat(16000F, 16, 1, true, false);
	
	public final UUID uid;
	public final Thread threadInput, threadOutput;
	public boolean isWorking = false;
	
	public final CachingInputStream playback = new CachingInputStream(1000);
	public final BlobbedOutputStream output = new BlobbedOutputStream(1000, this::sendVoiceBits);
	
	public boolean mute, deafen;
	
	public int trackLimit = 100;
	
	public FloatList incomingRMS = new FloatArrayList();
	public FloatList outgoingRMS = new FloatArrayList();
	
	public final AtomicBoolean sharingScreen = new AtomicBoolean(false);
	public FlowImageGL screenRemote = null;
	
	final ThreadPoolExecutor VOICE_PIPE = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
	
	public VoiceChatReceiver(UUID user)
	{
		if(VC != null)
		{
			HCNet.INSTANCE.sendToServer(new PacketStopVC().setVc(user));
			throw new IllegalStateException();
		}
		
		VC = this;
		
		this.uid = user;
		isWorking = true;
		
		threadInput = SidedThreadGroups.CLIENT.newThread(this::workIn);
		threadInput.setName("SIn-" + user.toString());
		threadInput.start();
		
		threadOutput = SidedThreadGroups.CLIENT.newThread(this::workOut);
		threadOutput.setName("SOut-" + user.toString());
		threadOutput.start();
	}
	
	public void shareScreen()
	{
		sharingScreen.set(true);
		
		try
		{
			Robot dolbaeb = new Robot();
			GraphicsDevice dev = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
			int givenW = dev.getDisplayMode().getWidth();
			int givenH = dev.getDisplayMode().getHeight();
			
			int maxWidth = 853;
			int maxHeight = 480;
			
			float aspectRatio = givenW / (float) givenH;
			
			int w = Math.min(maxWidth, givenW);
			int wh = ImageCompressor.getHeightByWidthNAR(maxWidth, aspectRatio);
			
			int h = Math.min(maxHeight, givenH);
			int hw = ImageCompressor.getWidthByHeightNAR(h, aspectRatio);
			
			BufferedImage target;
			
			if(w < hw)
			{
				h = w;
				w = wh;
			} else
				w = hw;
			
			HCNet.INSTANCE.sendToServer(new PacketSetupScreenShare().setSize(w, h).setTarget(uid));
			ScreenCapture.start(sharingScreen, 1024, gi ->
			{
				BufferedImage us = new BufferedImage(gi.getWidth(), gi.getHeight(), BufferedImage.TYPE_USHORT_555_RGB);
				Graphics2D g = us.createGraphics();
				g.drawImage(gi, 0, 0, null);
				g.dispose();
				for(int y = 0; y < gi.getHeight(); y += 4)
				{
					PacketTransportVideo vid = new PacketTransportVideo().setTarget(uid).setData((DataBufferUShort) us.getRaster().getDataBuffer(), us.getWidth(), y, 4);
					HCNet.INSTANCE.sendToServer(vid);
				}
			});
		} catch(AWTException e)
		{
			sharingScreen.set(false);
		}
	}
	
	public void writeData(byte[] data)
	{
		BlockingQueue<Runnable> queue = VOICE_PIPE.getQueue();
		
		// Unload large queues of voice bits!
		if(queue.size() > 2)
		{
			while(!queue.isEmpty())
				VOICE_PIPE.remove(queue.peek());
		}
		
		VOICE_PIPE.execute(() ->
		{
			if(deafen)
				for(int i = 0; i < data.length; ++i)
					data[i] = 0;
				
			float rms = AudioSampleHelper.getRMS(AudioSampleHelper.asSamples(data, data.length));
			incomingRMS.add(rms); // add last
			while(incomingRMS.size() > trackLimit)
				incomingRMS.remove(0); // remove first
				
			playback.write(data);
		});
	}
	
	public void closeChannel()
	{
		isWorking = false;
		VC = null;
		sharingScreen.set(false);
		
		threadInput.interrupt();
		threadOutput.interrupt();
		
		HCNet.INSTANCE.sendToServer(new PacketStopVC().setVc(uid));
	}
	
	private void workIn()
	{
		try
		{
			SourceDataLine speakers = AudioSystem.getSourceDataLine(format);
			
			speakers.open(format);
			speakers.start();
			byte[] data = new byte[1000];
			int read;
			while(isWorking && (read = playback.read(data)) > 0)
				try
				{
					if(read == data.length)
						speakers.write(data, 0, read);
				} catch(IllegalArgumentException iae)
				{
					// read % 2 != 0
				}
			speakers.drain();
			speakers.stop();
		} catch(LineUnavailableException e)
		{
			e.printStackTrace();
		}
	}
	
	private void workOut()
	{
		try
		{
			TargetDataLine microphone = AudioSystem.getTargetDataLine(format);
			
			microphone.open(format);
			microphone.start();
			
			byte[] buf = new byte[1000];
			int read;
			while(isWorking)
			{
				read = microphone.read(buf, 0, buf.length);
				output.write(buf, 0, read);
			}
			
			microphone.close();
		} catch(LineUnavailableException e)
		{
			e.printStackTrace();
		}
	}
	
	public void sendVoiceBits(byte[] data)
	{
		if(mute || deafen)
			Arrays.fill(data, (byte) 0);
		
		float rms = AudioSampleHelper.getRMS(AudioSampleHelper.asSamples(data, data.length));
		outgoingRMS.add(rms); // add last
		while(outgoingRMS.size() > trackLimit)
			outgoingRMS.remove(0); // remove first
			
		if(isWorking)
			HCNet.INSTANCE.sendToServer(new PacketTransferBytesVC().setVictim(uid).setVoice(data));
	}
}