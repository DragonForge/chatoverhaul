package com.zeitheron.chatoverhaul.client.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

import com.google.common.util.concurrent.AtomicDouble;
import com.zeitheron.hammercore.lib.zlib.audio.AudioSampleHelper;
import com.zeitheron.hammercore.lib.zlib.utils.Threading;

public class SoundIO
{
	public static final AudioFormat AUDIO_FORMAT = new AudioFormat(16000F, 16, 1, true, false);
	
	public static void play(InputStream input, int length, AtomicDouble progress)
	{
		try
		{
			SourceDataLine speakers = AudioSystem.getSourceDataLine(AUDIO_FORMAT);
			
			speakers.open(AUDIO_FORMAT);
			speakers.start();
			
			byte[] data = new byte[512];
			
			int read;
			
			int total = 0;
			
			while((read = input.read(data)) > 0)
			{
				speakers.write(data, 0, read);
				total += read;
				progress.set(Math.max(0, total / (double) length));
			}
			
			speakers.drain();
			speakers.stop();
		} catch(LineUnavailableException | IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void write(AtomicBoolean record, AtomicDouble rms, Consumer<byte[]>... onDone)
	{
		try
		{
			TargetDataLine microphone = AudioSystem.getTargetDataLine(AUDIO_FORMAT);
			
			microphone.open(AUDIO_FORMAT);
			microphone.start();
			
			Threading.createAndStart("MicRecorder0x" + Integer.toHexString(microphone.hashCode()), () ->
			{
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				
				byte[] buf = new byte[800];
				int read;
				while(record.get())
				{
					read = microphone.read(buf, 0, buf.length);
					rms.set(AudioSampleHelper.getRMS(AudioSampleHelper.asSamples(buf, read)));
					output.write(buf, 0, read);
				}
				microphone.close();
				
				// Now resume all waiting threads to send this data
				
				synchronized(record)
				{
					record.notifyAll();
				}
				
				for(Consumer<byte[]> c : onDone)
					c.accept(output.toByteArray());
			});
		} catch(LineUnavailableException e)
		{
			throw new IllegalStateException("Voice line is already in use!");
		}
	}
}