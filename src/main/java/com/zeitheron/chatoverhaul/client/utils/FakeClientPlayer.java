package com.zeitheron.chatoverhaul.client.utils;

import java.lang.reflect.Field;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.collect.Maps;
import com.mojang.authlib.GameProfile;
import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.stats.StatBase;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ITeleporter;

public class FakeClientPlayer extends EntityPlayerSP
{
	private static Map<GameProfile, FakeClientPlayer> fakePlayers = Maps.newHashMap();
	
	public static FakeClientPlayer get(GameProfile user)
	{
		if(!fakePlayers.containsKey(user))
			fakePlayers.put(user, new FakeClientPlayer(Minecraft.getMinecraft(), user));
		return fakePlayers.get(user);
	}
	
	public FakeClientPlayer(Minecraft mc, GameProfile profile)
	{
		super(mc, mc.world, mc.getConnection(), mc.player.getStatFileWriter(), mc.player.getRecipeBook());
		
		Field profileField = findFirstFieldOfType(GameProfile.class, EntityPlayer.class.getDeclaredFields());
		
		if(profileField == null)
			ChatOverhaul.LOG.warn("WARNING: Unable to set GameProfile!");
		
		try
		{
			FinalFieldHelper.setFinalField(profileField, this, profile);
		} catch(ReflectiveOperationException e)
		{
			ChatOverhaul.LOG.error("ERROR: Unable to set GameProfile!");
			e.printStackTrace();
		}
	}
	
	@Override
	public Vec3d getPositionVector()
	{
		return new Vec3d(0, 0, 0);
	}
	
	@Override
	public boolean canUseCommand(int i, String s)
	{
		return false;
	}
	
	@Override
	public void sendStatusMessage(ITextComponent chatComponent, boolean actionBar)
	{
	}
	
	@Override
	public void sendMessage(ITextComponent component)
	{
	}
	
	@Override
	public void addStat(StatBase par1StatBase, int par2)
	{
	}
	
	@Override
	public void openGui(Object mod, int modGuiId, World world, int x, int y, int z)
	{
	}
	
	@Override
	public boolean isEntityInvulnerable(DamageSource source)
	{
		return true;
	}
	
	@Override
	public boolean canAttackPlayer(EntityPlayer player)
	{
		return false;
	}
	
	@Override
	public void onDeath(DamageSource source)
	{
		return;
	}
	
	@Override
	public void onUpdate()
	{
		return;
	}
	
	@Override
	public Entity changeDimension(int dim, ITeleporter teleporter)
	{
		return this;
	}
	
	@Nullable
	public static Field findFirstFieldOfType(Class<?> type, Field... fields)
	{
		for(Field f : fields)
			if(type.isAssignableFrom(f.getType()))
			{
				f.setAccessible(true);
				return f;
			}
		return null;
	}
}