package com.zeitheron.chatoverhaul.client.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mojang.authlib.GameProfile;
import com.zeitheron.hammercore.client.utils.UtilsFX;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class RenderPlayerBody
{
	public static final Logger LOGGER = LogManager.getLogger();
	
	public static final ModelPlayer def = new ModelPlayer(0F, false), slim = new ModelPlayer(0F, true);
	
	public static void drawEntityOnScreen(int posX, int posY, float scale, float mouseX, float mouseY, PlayerRenderingInfo info)
	{
		if(Minecraft.getMinecraft().getConnection() == null || info == null || info.profile == null || info.profile.getId() == null)
			return;
		
		GlStateManager.enableColorMaterial();
		GlStateManager.pushMatrix();
		GlStateManager.translate((float) posX, (float) posY, 50.0F);
		GlStateManager.scale((float) (-scale), (float) scale, (float) scale);
		GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
		float f = info.renderYawOffset;
		float f1 = info.rotationYaw;
		float f2 = info.rotationPitch;
		float f3 = info.prevRotationYawHead;
		float f4 = info.rotationYawHead;
		
		GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
		
		GlStateManager.rotate(-((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
		
		info.renderYawOffset = (float) Math.atan((double) (mouseX / 40.0F)) * 20.0F + 180;
		info.rotationYaw = (float) Math.atan((double) (mouseX / 40.0F)) * 40.0F + 180;
		info.rotationPitch = -((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F;
		info.rotationYawHead = info.rotationYaw;
		info.prevRotationYawHead = info.rotationYaw;
		GlStateManager.translate(0.0F, 0.0F, 0.0F);
		RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
		rendermanager.setPlayerViewY(180.0F);
		rendermanager.setRenderShadow(false);
		
		NetworkPlayerInfo networkplayerinfo = Minecraft.getMinecraft().getConnection().getPlayerInfo(info.profile.getId());
		ResourceLocation texture = networkplayerinfo == null ? DefaultPlayerSkin.getDefaultSkin(info.profile.getId()) : networkplayerinfo.getLocationSkin();
		
		UtilsFX.bindTexture(texture);
		renderPlayer(info.isSlim(), 1F, info);
		
		rendermanager.setRenderShadow(true);
		info.renderYawOffset = f;
		info.rotationYaw = f1;
		info.rotationPitch = f2;
		info.prevRotationYawHead = f3;
		info.rotationYawHead = f4;
		GlStateManager.popMatrix();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableRescaleNormal();
		GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GlStateManager.disableTexture2D();
		GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}
	
	public static void renderPlayer(boolean slim, float partialTicks, PlayerRenderingInfo info)
	{
		ModelPlayer model = slim ? RenderPlayerBody.slim : def;
		
		FakeClientPlayer sp = FakeClientPlayer.get(info.profile);
		
		sp.prevRotationYawHead = info.rotationYawHead;
		sp.rotationYaw = info.rotationYaw;
		sp.rotationPitch = info.rotationPitch;
		sp.renderYawOffset = info.renderYawOffset;
		sp.setSneaking(info.isSitting);
		
		{
			GlStateManager.pushMatrix();
			GlStateManager.disableCull();
			model.swingProgress = info.getSwingProgress(partialTicks);
			model.isRiding = info.isSitting;
			model.isChild = info.isChild;
			
			try
			{
				float f = interpolateRotation(info.prevRenderYawOffset, info.renderYawOffset, partialTicks);
				float f1 = interpolateRotation(info.prevRotationYawHead, info.rotationYawHead, partialTicks);
				float f2 = f1 - f;
				
				float f7 = info.prevRotationPitch + (info.rotationPitch - info.prevRotationPitch) * partialTicks;
				
				float f8 = handleRotationFloat(partialTicks);
				
				float f4 = prepareScale(partialTicks);
				float f5 = 0.0F;
				float f6 = 0.0F;
				
				GlStateManager.rotate(f, 0, 1, 0);
				
				if(!info.isSitting)
				{
					f5 = info.prevLimbSwingAmount + (info.limbSwingAmount - info.prevLimbSwingAmount) * partialTicks;
					f6 = info.limbSwing - info.limbSwingAmount * (1.0F - partialTicks);
					
					if(info.isChild)
					{
						f6 *= 3.0F;
					}
					
					if(f5 > 1.0F)
					{
						f5 = 1.0F;
					}
					f2 = f1 - f; // Forge: Fix MC-1207
				}
				
				GlStateManager.enableAlpha();
				model.setLivingAnimations(sp, f6, f5, partialTicks);
				model.setRotationAngles(f6, f5, f8, f2, f7, f4, sp);
				
				if(info.renderOutlines)
				{
					GlStateManager.enableColorMaterial();
					GlStateManager.enableOutlineMode(16777215);
					
					// if(!(entity instanceof EntityPlayer) || !((EntityPlayer)
					// entity).isSpectator())
					// {
					// this.renderLayers(entity, f6, f5, partialTicks, f8, f2,
					// f7, f4);
					// }
					
					GlStateManager.disableOutlineMode();
					GlStateManager.disableColorMaterial();
				} else
				{
					// boolean flag = this.setDoRenderBrightness(entity,
					// partialTicks);
					
					renderModel(info, model, f6, f5, f8, f2, f7, f4);
					
					// if(flag)
					// {
					// unsetBrightness();
					// }
					
					GlStateManager.depthMask(true);
					
					// if(!(entity instanceof EntityPlayer) || !((EntityPlayer)
					// entity).isSpectator())
					// {
					// this.renderLayers(entity, f6, f5, partialTicks, f8, f2,
					// f7, f4);
					// }
				}
				
				GlStateManager.disableRescaleNormal();
			} catch(Exception exception)
			{
				LOGGER.error("Couldn't render entity", (Throwable) exception);
			}
			
			GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
			GlStateManager.enableTexture2D();
			GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
			GlStateManager.enableCull();
			GlStateManager.popMatrix();
		}
	}
	
	protected static void renderModel(PlayerRenderingInfo info, ModelBase model, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor)
	{
		if(info.ghost)
			GlStateManager.enableBlendProfile(GlStateManager.Profile.TRANSPARENT_MODEL);
		
		FakeClientPlayer fcl = FakeClientPlayer.get(info.profile);
		
		fcl.prevRotationYawHead = info.rotationYawHead;
		fcl.rotationYaw = info.rotationYaw;
		fcl.rotationPitch = info.rotationPitch;
		
		model.render(fcl, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
		
		if(info.ghost)
			GlStateManager.disableBlendProfile(GlStateManager.Profile.TRANSPARENT_MODEL);
	}
	
	protected static float handleRotationFloat(float partialTicks)
	{
		return (float) (System.currentTimeMillis() % 100000L) / 50L + partialTicks;
	}
	
	protected static float interpolateRotation(float prevYawOffset, float yawOffset, float partialTicks)
	{
		float f;
		
		for(f = yawOffset - prevYawOffset; f < -180.0F; f += 360.0F)
		{
			;
		}
		
		while(f >= 180.0F)
		{
			f -= 360.0F;
		}
		
		return prevYawOffset + partialTicks * f;
	}
	
	public static float prepareScale(float partialTicks)
	{
		GlStateManager.enableRescaleNormal();
		GlStateManager.scale(-1.0F, -1.0F, 1.0F);
		float f = 0.0625F;
		GlStateManager.translate(0.0F, -1.501F, 0.0F);
		return 0.0625F;
	}
	
	public static class PlayerRenderingInfo
	{
		public GameProfile profile;
		public float rotationPitch, prevRotationPitch;
		public float rotationYaw, prevYaw;
		public float rotationYawHead, prevRotationYawHead;
		public float renderYawOffset, prevRenderYawOffset;
		public float swingProgress, prevSwingProgress;
		public float limbSwing, limbSwingAmount, prevLimbSwingAmount;
		public boolean isSitting, isChild, renderOutlines, ghost;
		
		public void update()
		{
			prevRotationPitch = rotationPitch;
			prevYaw = rotationYaw;
			prevRotationYawHead = rotationYawHead;
			prevRenderYawOffset = renderYawOffset;
			prevSwingProgress = swingProgress;
			prevLimbSwingAmount = limbSwingAmount;
		}
		
		public boolean isSlim()
		{
			return false;
		}
		
		@SideOnly(Side.CLIENT)
		public float getSwingProgress(float partialTickTime)
		{
			float f = this.swingProgress - this.prevSwingProgress;
			if(f < 0.0F)
				++f;
			return this.prevSwingProgress + f * partialTickTime;
		}
	}
}