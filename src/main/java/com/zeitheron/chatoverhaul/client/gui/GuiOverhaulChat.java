package com.zeitheron.chatoverhaul.client.gui;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

import com.zeitheron.chatoverhaul.api.CustomMessageCreateEvent;
import com.zeitheron.chatoverhaul.api.IRenderableChatLine;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineNew;
import com.zeitheron.hammercore.utils.FinalFieldHelper;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiUtilRenderComponents;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.MinecraftForge;

public class GuiOverhaulChat extends GuiNewChat
{
	public GuiOverhaulChat()
	{
		super(Minecraft.getMinecraft());
	}
	
	public static int mouseX, mouseY, relSelX, relSelY;
	public static ChatLine selectedLine;
	
	@Override
	public void drawChat(int updateCounter)
	{
		selectedLine = null;
		
		if(this.mc.gameSettings.chatVisibility != EntityPlayer.EnumChatVisibility.HIDDEN)
		{
			int i = this.getChatHeight();
			int j = this.drawnChatLines.size();
			float f = this.mc.gameSettings.chatOpacity * 0.9F + 0.1F;
			
			if(j > 0)
			{
				boolean flag = false;
				
				if(this.getChatOpen())
					flag = true;
				
				float f1 = this.getChatScale();
				int k = MathHelper.ceil((float) this.getChatWidth() / f1);
				GlStateManager.pushMatrix();
				GlStateManager.scale(f1, f1, 1.0F);
				int l = 0;
				
				int y = -1;
				int yoff = 0;
				
				for(int i1 = 0; i1 + this.scrollPos < this.drawnChatLines.size() && yoff < i; ++i1)
				{
					int ind = i1 + this.scrollPos;
					
					ChatLine chatline = this.drawnChatLines.get(ind);
					
					if(chatline.getClass() == ChatLine.class)
					{
						ChatLineNew cln = new ChatLineNew(chatline.getUpdatedCounter(), chatline.getChatComponent(), chatline.getChatLineID());
						if(MinecraftForge.EVENT_BUS.post(new CustomMessageCreateEvent(cln)))
						{
							this.drawnChatLines.remove(ind);
							--i1;
							continue;
						} else
							this.drawnChatLines.set(ind, cln);
					}
					
					if(chatline instanceof IRenderableChatLine)
					{
						IRenderableChatLine rcl = (IRenderableChatLine) chatline;
						
						if(chatline.getUpdatedCounter() == 0)
						{
							int updCounter = Minecraft.getMinecraft().ingameGUI.getUpdateCounter();
							try
							{
								FinalFieldHelper.setFinalField(ChatLine.class.getDeclaredFields()[0], chatline, updCounter);
							} catch(SecurityException | ReflectiveOperationException e)
							{
								e.printStackTrace();
							}
						}
						
						int j1 = updateCounter - chatline.getUpdatedCounter();
						
						if(j1 < 200 || flag)
						{
							double d0 = (double) j1 / 200.0D;
							d0 = 1.0D - d0;
							d0 = d0 * 10.0D;
							d0 = MathHelper.clamp(d0, 0.0D, 1.0D);
							d0 = d0 * d0;
							int l1 = (int) (255.0D * d0);
							if(flag)
								l1 = 255;
							l1 = (int) ((float) l1 * f);
							++l;

							GuiScreen gs = Minecraft.getMinecraft().currentScreen;
							int ay = 0;
							if(gs != null)
								ay = gs.height - 46;
							
							if(mouseX >= 0 && mouseY >= ay + y - rcl.getHeight() + 9 && mouseX < k && mouseY < ay + y + 9)
								selectedLine = chatline;
							
							rcl.render(l1, k, y);
							
							y -= rcl.getHeight();
							yoff += rcl.getHeight();
						}
					}
					
					if(yoff > i)
						break;
				}
				
				if(flag)
				{
					int k2 = this.mc.fontRenderer.FONT_HEIGHT;
					GlStateManager.translate(-3.0F, 0.0F, 0.0F);
					int l2 = j * k2 + j;
					int i3 = l * k2 + l;
					int j3 = this.scrollPos * i3 / j;
					int k1 = i3 * i3 / l2;
					
					if(l2 != i3)
					{
						int k3 = j3 > 0 ? 170 : 96;
						int l3 = this.isScrolled ? 13382451 : 3355562;
						drawRect(0, -j3, 2, -j3 - k1, l3 + (k3 << 24));
						drawRect(2, -j3, 1, -j3 - k1, 13421772 + (k3 << 24));
					}
				}
				
				GlStateManager.popMatrix();
			}
		}
	}
	
	@Override
	public void printChatMessage(ITextComponent chatComponent)
	{
		this.printChatMessageWithOptionalDeletion(chatComponent, 0);
	}
	
	/**
	 * prints the ChatComponent to Chat. If the ID is not 0, deletes an existing
	 * Chat Line of that ID from the GUI
	 */
	@Override
	public void printChatMessageWithOptionalDeletion(ITextComponent chatComponent, int chatLineId)
	{
		this.setChatLine$(chatComponent, chatLineId, this.mc.ingameGUI.getUpdateCounter(), false);
		LOGGER.info("[CHAT] {}", (Object) chatComponent.getUnformattedText().replaceAll("\r", "\\\\r").replaceAll("\n", "\\\\n"));
	}
	
	private void setChatLine$(ITextComponent chatComponent, int chatLineId, int updateCounter, boolean displayOnly)
	{
		if(chatLineId != 0)
		{
			this.deleteChatLine(chatLineId);
		}
		
		int i = MathHelper.floor((float) this.getChatWidth() / this.getChatScale());
		List<ITextComponent> list = GuiUtilRenderComponents.splitText(chatComponent, i, this.mc.fontRenderer, false, false);
		boolean flag = this.getChatOpen();
		
		for(ITextComponent itextcomponent : list)
		{
			if(flag && this.scrollPos > 0)
			{
				this.isScrolled = true;
				this.scroll(1);
			}
			
			this.drawnChatLines.add(0, new ChatLine(updateCounter, itextcomponent, chatLineId));
		}
		
		while(this.drawnChatLines.size() > 100)
		{
			this.drawnChatLines.remove(this.drawnChatLines.size() - 1);
		}
		
		if(!displayOnly)
		{
			this.chatLines.add(0, new ChatLine(updateCounter, chatComponent, chatLineId));
			
			while(this.chatLines.size() > 100)
			{
				this.chatLines.remove(this.chatLines.size() - 1);
			}
		}
	}
	
	@Override
	public void refreshChat()
	{
		this.drawnChatLines.clear();
		this.resetScroll();
		
		for(int i = this.chatLines.size() - 1; i >= 0; --i)
		{
			ChatLine chatline = this.chatLines.get(i);
			this.setChatLine$(chatline.getChatComponent(), chatline.getChatLineID(), chatline.getUpdatedCounter(), true);
		}
	}
	
	/**
	 * Gets the chat component under the mouse
	 */
	@Nullable
	@Override
	public ITextComponent getChatComponent(int mouseX, int mouseY)
	{
		if(!this.getChatOpen())
		{
			return null;
		} else
		{
			ScaledResolution scaledresolution = new ScaledResolution(this.mc);
			int i = scaledresolution.getScaleFactor();
			float f = this.getChatScale();
			int j = mouseX / i - 2;
			j = MathHelper.floor((float) j / f);
			
			if(j >= 0 && selectedLine != null)
			{
				int l = Math.min(this.getLineCount(), this.drawnChatLines.size());
				
				if(j <= MathHelper.floor((float) this.getChatWidth() / this.getChatScale()))
				{
					ChatLine chatline = selectedLine;
					int j1 = 0;
					
					IRenderableChatLine ircl = WorldUtil.cast(chatline, IRenderableChatLine.class);
					
					if(chatline.getChatComponent() != null)
						for(ITextComponent itextcomponent : chatline.getChatComponent())
							if(itextcomponent instanceof TextComponentString)
							{
								j1 += this.mc.fontRenderer.getStringWidth(GuiUtilRenderComponents.removeTextColorsIfConfigured(((TextComponentString) itextcomponent).getText(), false));
								
								if(j1 > j)
									return ircl != null ? ircl.transformHoveredComponent(itextcomponent) : itextcomponent;
							}
				} else
					return null;
			} else
				return null;
		}
		
		return null;
	}
	
	/**
	 * Returns true if the chat GUI is open
	 */
	@Override
	public boolean getChatOpen()
	{
		return this.mc.currentScreen instanceof GuiChat;
	}
	
	/**
	 * finds and deletes a Chat line by ID
	 */
	@Override
	public void deleteChatLine(int id)
	{
		Iterator<ChatLine> iterator = this.drawnChatLines.iterator();
		
		while(iterator.hasNext())
		{
			ChatLine chatline = iterator.next();
			
			if(chatline.getChatLineID() == id)
			{
				iterator.remove();
			}
		}
		
		iterator = this.chatLines.iterator();
		
		while(iterator.hasNext())
		{
			ChatLine chatline1 = iterator.next();
			
			if(chatline1.getChatLineID() == id)
			{
				iterator.remove();
				break;
			}
		}
	}
	
	@Override
	public int getChatWidth()
	{
		return calculateChatboxWidth(this.mc.gameSettings.chatWidth);
	}
	
	@Override
	public int getChatHeight()
	{
		return calculateChatboxHeight(this.getChatOpen() ? this.mc.gameSettings.chatHeightFocused : this.mc.gameSettings.chatHeightUnfocused);
	}
	
	/**
	 * Returns the chatscale from mc.gameSettings.chatScale
	 */
	@Override
	public float getChatScale()
	{
		return this.mc.gameSettings.chatScale;
	}
	
	@Override
	public int getLineCount()
	{
		return this.getChatHeight() / 9;
	}
}