package com.zeitheron.chatoverhaul.client.gui;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import org.lwjgl.input.Keyboard;

import com.google.common.util.concurrent.AtomicDouble;
import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.api.IChatInteractiveWidget;
import com.zeitheron.chatoverhaul.api.IRenderableChatLine;
import com.zeitheron.chatoverhaul.client.ote.OTEffect;
import com.zeitheron.chatoverhaul.client.ote.OnTopEffects;
import com.zeitheron.chatoverhaul.client.utils.RenderPlayerBody;
import com.zeitheron.chatoverhaul.client.utils.RenderPlayerBody.PlayerRenderingInfo;
import com.zeitheron.chatoverhaul.client.utils.SoundIO;
import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.chatoverhaul.net.APacketRemCL;
import com.zeitheron.chatoverhaul.net.NetTransportSendChatLine;
import com.zeitheron.chatoverhaul.net.PacketSendTyping;
import com.zeitheron.chatoverhaul.net.PacketStopTyping;
import com.zeitheron.chatoverhaul.net.vc.PacketStopScreenShare;
import com.zeitheron.chatoverhaul.utils.ImageCompressor;
import com.zeitheron.hammercore.client.HCClientOptions;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;
import com.zeitheron.hammercore.net.HCNet;

import it.unimi.dsi.fastutil.floats.FloatList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class GuiChatCO extends GuiChat
{
	public String queueSetText;
	
	public final AtomicBoolean recording = new AtomicBoolean(false);
	public final AtomicDouble rms = new AtomicDouble();
	
	public double vol, prevVol;
	
	public boolean send = true;
	
	DynGuiTex callTex;
	
	public Integer anchorX, anchorY;
	
	@Override
	public void initGui()
	{
		String setText = inputField == null ? queueSetText : inputField.getText();
		super.initGui();
		if(setText != null)
			inputField.setText(setText);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		GuiOverhaulChat.mouseX = mouseX;
		GuiOverhaulChat.mouseY = mouseY;
		
		boolean hover = mouseX >= 2 && mouseY >= this.height - 28 && mouseX < 42 && mouseY < this.height - 17;
		
		String ln = I18n.format("chatoverhaul.attach.file");
		drawRect(2, this.height - 28, 42, this.height - 17, Integer.MIN_VALUE);
		fontRenderer.drawString(ln, 2 + (40 - fontRenderer.getStringWidth(ln)) / 2, this.height - 26, hover ? 0xFFFF66 : 0xFFFFFF);
		
		hover = mouseX >= 46 && mouseY >= this.height - 28 && mouseX < 88 && mouseY < this.height - 17;
		ln = I18n.format("chatoverhaul.attach.image");
		drawRect(46, this.height - 28, 88, this.height - 17, Integer.MIN_VALUE);
		fontRenderer.drawString(ln, 46 + (40 - fontRenderer.getStringWidth(ln)) / 2, this.height - 26, hover ? 0xFFFF66 : 0xFFFFFF);
		
		hover = mouseX >= 92 && mouseY >= this.height - 28 && mouseX < 132 && mouseY < this.height - 17;
		ln = I18n.format("chatoverhaul.attach.voice");
		drawRect(92, this.height - 28, 132, this.height - 17, Integer.MIN_VALUE);
		fontRenderer.drawString(ln, 92 + (40 - fontRenderer.getStringWidth(ln)) / 2, this.height - 26, hover ? 0xFFFF66 : 0xFFFFFF);
		GlStateManager.enableBlend();
		ChatOverhaul.proxy.bindTexture("recording" + (recording.get() ? "" : "_inactive"));
		RenderUtil.drawFullTexturedModalRect(92, this.height - 28, 11, 11);
		
		vc: if(VoiceChatReceiver.VC != null && mc.getConnection() != null)
		{
			NetworkPlayerInfo npi = mc.getConnection().getPlayerInfo(VoiceChatReceiver.VC.uid);
			
			if(npi == null)
				break vc;
			
			if(callTex == null)
			{
				GuiTexBakery b = new GuiTexBakery();
				b.body(0, 0, 166, 54);
				callTex = b.bake();
			}
			
			callTex.render(width - 168, -4);
			
			PlayerRenderingInfo pri = new PlayerRenderingInfo();
			pri.profile = npi.getGameProfile();
			
			GlStateManager.color(1F, 1F, 1F, 1F);
			RenderPlayerBody.drawEntityOnScreen(width - 168 + 18, 13 + 30, 20F, 0, 0, pri);
			
			GlStateManager.enableBlend();
			
			VoiceChatReceiver vc = VoiceChatReceiver.VC;
			
			boolean muteHov = mouseX >= width - 168 + 36 && mouseX < width - 168 + 60 && mouseY < 24;
			
			if(vc.mute || vc.deafen)
			{
				GlStateManager.color(1F, 1F, muteHov ? .8F : 1F, 1F);
				
				ChatOverhaul.proxy.bindTexture("microphone_off_main");
				RenderUtil.drawFullTexturedModalRect(width - 168 + 36, 0, 24, 24);
				
				GlStateManager.color(1F, .25F, muteHov ? 0F : .25F, 1F);
				
				ChatOverhaul.proxy.bindTexture("microphone_off_overlay");
				RenderUtil.drawFullTexturedModalRect(width - 168 + 36, 0, 24, 24);
			} else
			{
				GlStateManager.color(1F, 1F, muteHov ? .8F : 1F, 1F);
				ChatOverhaul.proxy.bindTexture("microphone");
				RenderUtil.drawFullTexturedModalRect(width - 168 + 36, 0, 24, 24);
			}
			
			int sx = width - 168 + 36 + 26;
			int sy = 26;
			
			FloatList inc = vc.incomingRMS;
			for(int i = 0; i < inc.size(); ++i)
			{
				float vol = Math.min(1, Math.max(0, inc.getFloat(i)) * 8F);
				vol = vol * vol;
				
				Gui.drawRect(sx + i, sy + 20 - Math.round(vol * 21), sx + i + 1, sy + 21, 0x66EEFFFF);
			}
			
			sy = 2;
			inc = vc.outgoingRMS;
			for(int i = 0; i < inc.size(); ++i)
			{
				float vol = Math.min(1, Math.max(0, inc.getFloat(i)) * 8F);
				vol = vol * vol;
				
				Gui.drawRect(sx + i, sy + 20 - Math.round(vol * 21), sx + i + 1, sy + 21, 0x66EEFFFF);
			}
			
			boolean deafHov = mouseX >= width - 168 + 36 && mouseY >= 24 && mouseX < width - 168 + 60 && mouseY < 48;
			
			if(vc.deafen)
			{
				GlStateManager.color(1F, 1F, deafHov ? .8F : 1F, 1F);
				
				ChatOverhaul.proxy.bindTexture("speakers");
				RenderUtil.drawFullTexturedModalRect(width - 168 + 36, 24, 24, 24);
				
				GlStateManager.color(1F, .25F, deafHov ? 0F : .25F, 1F);
				
				ChatOverhaul.proxy.bindTexture("speakers_mute");
				RenderUtil.drawFullTexturedModalRect(width - 168 + 36, 24, 24, 24);
			} else
			{
				GlStateManager.color(1F, 1F, deafHov ? .8F : 1F, 1F);
				
				ChatOverhaul.proxy.bindTexture("speakers");
				RenderUtil.drawFullTexturedModalRect(width - 168 + 36, 24, 24, 24);
				
				ChatOverhaul.proxy.bindTexture("speakers_sound");
				RenderUtil.drawFullTexturedModalRect(width - 168 + 36, 24, 24, 24);
			}
			
			int w = 124;
			
			ChatOverhaul.proxy.bindTexture("alt_widgets");
			
			{
				String draw = I18n.format("chatoverhaul.vc.ss." + (vc.sharingScreen.get() ? "off" : "on"));
				int dy = 50;
				int dx = (width - 168) + (168 - w) / 2;
				boolean end = mouseX >= dx && mouseY >= dy && mouseX < dx + w && mouseY < dy + 14;
				GlStateManager.color(end ? .125F : .25F, end ? .8F : 1F, end ? .8F : 1F, 1F);
				RenderUtil.drawTexturedModalRect(dx, dy, 0, 60, 3, 14);
				RenderUtil.drawTexturedModalRect(dx + w - 3, dy, 6, 60, 3, 14);
				GlStateManager.pushMatrix();
				GlStateManager.translate(dx + 3, 0, 0);
				GlStateManager.scale(w - 6, 1, 1);
				RenderUtil.drawTexturedModalRect(0, dy, 4, 60, 1, 14);
				GlStateManager.popMatrix();
				fontRenderer.drawString(draw, dx + (w - fontRenderer.getStringWidth(draw)) / 2, dy + 3, 0xFFFFFFFF, false);
			}
			
			ChatOverhaul.proxy.bindTexture("alt_widgets");
			
			{
				String draw = I18n.format("chatoverhaul.vc.end");
				int dx = (width - 168) + (168 - w) / 2;
				int dy = 64;
				boolean end = mouseX >= dx && mouseY >= dy && mouseX < dx + w && mouseY < dy + 14;
				GlStateManager.color(end ? .8F : 1F, end ? .125F : .25F, end ? .125F : .25F, 1F);
				RenderUtil.drawTexturedModalRect(dx, dy, 0, 60, 3, 14);
				RenderUtil.drawTexturedModalRect(dx + w - 3, dy, 6, 60, 3, 14);
				GlStateManager.pushMatrix();
				GlStateManager.translate(dx + 3, 0, 0);
				GlStateManager.scale(w - 6, 1, 1);
				RenderUtil.drawTexturedModalRect(0, dy, 4, 60, 1, 14);
				GlStateManager.popMatrix();
				fontRenderer.drawString(draw, dx + (w - fontRenderer.getStringWidth(draw)) / 2, dy + 3, 0xFFFFFFFF, false);
			}
			
			if(vc.screenRemote != null)
			{
				GlStateManager.bindTexture(vc.screenRemote.glId);
				
				float ar = vc.screenRemote.image.getWidth() / (float) vc.screenRemote.image.getHeight();
				RenderUtil.drawFullTexturedModalRect(0, 0, width / 4, ImageCompressor.getHeightByWidthNAR(width / 4, ar));
			}
		}
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		boolean hover = mouseX >= 2 && mouseY >= this.height - 28 && mouseX < 42 && mouseY < this.height - 17;
		
		if(hover && mouseButton == 0)
		{
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
			mc.displayGuiScreen(new GuiSelectUploadFile());
		}
		
		hover = mouseX >= 46 && mouseY >= this.height - 28 && mouseX < 88 && mouseY < this.height - 17;
		
		if(hover && mouseButton == 0)
		{
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
			mc.displayGuiScreen(new GuiSelectUploadImage());
		}
		
		hover = mouseX >= 92 && mouseY >= this.height - 28 && mouseX < 132 && mouseY < this.height - 17;
		
		if(!recording.get() && hover && mouseButton == 0)
		{
			recording.set(true);
			try
			{
				SoundIO.write(recording, rms, data ->
				{
					if(send)
					{
						NBTTagCompound nbt = new NBTTagCompound();
						nbt.setByteArray("Entity", data);
						NetTransportSendChatLine.send(ChatOverhaul.HANDLES.getValue(new ResourceLocation("chatoverhaul", "voice")), nbt);
					}
				});
			} catch(IllegalStateException ise)
			{
				recording.set(false);
				TextComponentString s = new TextComponentString(ise.getLocalizedMessage());
				s.getStyle().setColor(TextFormatting.RED);
				mc.player.sendMessage(s);
			}
		}
		
		if(mouseButton == 0 && GuiOverhaulChat.selectedLine instanceof IRenderableChatLine)
		{
			IRenderableChatLine ircl = (IRenderableChatLine) GuiOverhaulChat.selectedLine;
			ircl.handleMouseClick(mouseX, mouseY);
		}
		
		for(int i = 0; i < OnTopEffects.effects.size(); ++i)
		{
			OTEffect ote = OnTopEffects.effects.get(i);
			if(ote instanceof IChatInteractiveWidget && mouseX >= ote.x && mouseY >= ote.y && mouseX < ote.x + ote.xSize && mouseY < ote.y + ote.ySize)
				((IChatInteractiveWidget) ote).mouseClick((int) Math.round(mouseX - ote.x), (int) Math.round(mouseY - ote.y), mouseButton);
		}
		
		if(mouseButton == 0 && VoiceChatReceiver.VC != null && mouseX >= width - 168 + 36 && mouseX < width - 168 + 60 && mouseY < 24)
		{
			if(VoiceChatReceiver.VC.deafen && !VoiceChatReceiver.VC.mute)
				VoiceChatReceiver.VC.deafen = false;
			else
				VoiceChatReceiver.VC.mute = !VoiceChatReceiver.VC.mute;
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
		}
		
		if(mouseButton == 0 && VoiceChatReceiver.VC != null && mouseX >= width - 168 + 36 && mouseY >= 24 && mouseX < width - 168 + 60 && mouseY < 48)
		{
			VoiceChatReceiver.VC.deafen = !VoiceChatReceiver.VC.deafen;
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
		}
		
		int w = 124;
		int dx = (width - 168) + (168 - w) / 2;
		
		if(mouseButton == 0 && VoiceChatReceiver.VC != null && mouseX >= dx && mouseY >= 50 && mouseX < dx + w && mouseY < 64)
		{
			if(VoiceChatReceiver.VC.sharingScreen.get())
			{
				VoiceChatReceiver.VC.sharingScreen.set(false);
				HCNet.INSTANCE.sendToServer(new PacketStopScreenShare().setTarget(VoiceChatReceiver.VC.uid));
			} else
				VoiceChatReceiver.VC.shareScreen();
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
		}
		
		if(mouseButton == 0 && VoiceChatReceiver.VC != null && mouseX >= dx && mouseY >= 64 && mouseX < dx + w && mouseY < 78)
		{
			VoiceChatReceiver.VC.closeChannel();
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
		}
		
		if(mouseButton == 0 && VoiceChatReceiver.VC != null && VoiceChatReceiver.VC.screenRemote != null && VoiceChatReceiver.VC.screenRemote.image != null)
		{
			VoiceChatReceiver vc = VoiceChatReceiver.VC;
			
			float ar = vc.screenRemote.image.getWidth() / (float) vc.screenRemote.image.getHeight();
			if(mouseX >= 0 && mouseY >= 0 && mouseX < width / 4 && mouseY < ImageCompressor.getHeightByWidthNAR(width / 4, ar))
			{
				mc.displayGuiScreen(new GuiViewImageGL(vc.screenRemote.glId, ar));
				mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
			}
		}
		
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		
		prevVol = vol;
		vol = rms.get();
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
	}
	
	@Override
	protected void mouseReleased(int mouseX, int mouseY, int state)
	{
		super.mouseReleased(mouseX, mouseY, state);
		
		if(recording.get() && state == 0)
		{
			send = true;
			recording.set(false);
		}
	}
	
	@Override
	public void onGuiClosed()
	{
		if(recording.get())
		{
			send = false;
			recording.set(false);
			mc.player.sendStatusMessage(new TextComponentString(TextFormatting.DARK_RED + "Voice Recording Canceled"), true);
		}
		
		super.onGuiClosed();
	}
	
	@Override
	public void sendChatMessage(String msg, boolean addToChat)
	{
		HCClientOptions.getOptions().getCustomData().removeTag("ChatOverhaulLastText");
		HCClientOptions.getOptions().save();
		super.sendChatMessage(msg, addToChat);
		inputField.setText("");
		HCNet.INSTANCE.sendToServer(new PacketStopTyping());
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(keyCode == Keyboard.KEY_DELETE && GuiOverhaulChat.selectedLine instanceof IRenderableChatLine)
		{
			ChatFingerprint cf = ((IRenderableChatLine) GuiOverhaulChat.selectedLine).getPrint();
			if(cf != null)
				HCNet.INSTANCE.sendToServer(new APacketRemCL().setPrint(cf));
			else
			{
				List<ChatLine> cls = Minecraft.getMinecraft().ingameGUI.getChatGUI().chatLines;
				for(int i = 0; i < cls.size(); ++i)
				{
					ChatLine cl = cls.get(i);
					if(cl instanceof IRenderableChatLine && Objects.equals(cl, GuiOverhaulChat.selectedLine))
					{
						cls.remove(i);
						break;
					}
				}
				
				cls = Minecraft.getMinecraft().ingameGUI.getChatGUI().drawnChatLines;
				for(int i = 0; i < cls.size(); ++i)
				{
					ChatLine cl = cls.get(i);
					if(cl instanceof IRenderableChatLine && Objects.equals(cl, GuiOverhaulChat.selectedLine))
					{
						cls.remove(i);
						break;
					}
				}
			}
		}
		
		String prev = inputField.getText();
		super.keyTyped(typedChar, keyCode);
		if(!inputField.getText().equals(prev) && !inputField.getText().isEmpty())
			HCNet.INSTANCE.sendToServer(new PacketSendTyping());
	}
}