package com.zeitheron.chatoverhaul.client.gui;

import java.awt.Component;
import java.awt.HeadlessException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;

import com.google.common.util.concurrent.AtomicDouble;
import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.client.ote.OTEUploadFile;
import com.zeitheron.chatoverhaul.client.ote.OnTopEffects;
import com.zeitheron.chatoverhaul.net.NetTransportSendChatLine;
import com.zeitheron.chatoverhaul.net.PacketSendChatLine;
import com.zeitheron.chatoverhaul.utils.DataFileHostAPI;
import com.zeitheron.chatoverhaul.utils.ImageCompressor;
import com.zeitheron.chatoverhaul.utils.DataFileHostAPI.DataFileHostEntity;
import com.zeitheron.hammercore.lib.zlib.utils.Threading;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class GuiSelectUploadImage extends GuiScreen
{
	public GuiScreen parent;
	
	{
		if(parent == null)
			parent = Minecraft.getMinecraft().currentScreen;
		
		Threading.createAndStart(() ->
		{
			try
			{
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e)
			{
				e.printStackTrace();
			}
			
			JFileChooser jfc = new JFileChooser()
			{
				@Override
				protected JDialog createDialog(Component parent) throws HeadlessException
				{
					JDialog jd = super.createDialog(parent);
					jd.setAlwaysOnTop(true);
					jd.requestFocus();
					return jd;
				}
			};
			
			jfc.setFileFilter(new FileFilter()
			{
				@Override
				public String getDescription()
				{
					return "PNG/JPG Images";
				}
				
				@Override
				public boolean accept(File f)
				{
					return f.isDirectory() || (f.isFile() && (f.getName().endsWith(".png") || f.getName().endsWith(".jpeg") || f.getName().endsWith(".jpg")));
				}
			});
			
			int result = jfc.showOpenDialog(null);
			
			Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().displayGuiScreen(new GuiChat()));
			
			if(result == JFileChooser.APPROVE_OPTION)
			{
				try
				{
					NBTTagCompound data = new NBTTagCompound();
					ByteArrayOutputStream output = new ByteArrayOutputStream();
					ImageIO.write(ImageCompressor.scaleDownTo(4096, 4096, ImageIO.read(jfc.getSelectedFile())), "png", output);
					data.setByteArray("Entity", output.toByteArray());
					NetTransportSendChatLine.send(ChatOverhaul.HANDLES.getValue(new ResourceLocation("chatoverhaul", "image")), data);
					output.reset();
				} catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		// do not react to esc keys
	}
}