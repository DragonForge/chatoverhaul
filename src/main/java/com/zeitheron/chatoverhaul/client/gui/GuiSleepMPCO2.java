package com.zeitheron.chatoverhaul.client.gui;

import java.io.IOException;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;
import net.minecraft.network.play.client.CPacketEntityAction;
import net.minecraft.network.play.server.SPacketAnimation;

public class GuiSleepMPCO2 extends GuiChatCO
{
	@Override
	public void initGui()
	{
		super.initGui();
		this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height - 40, I18n.format("multiplayer.stopSleeping")));
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(keyCode == 1)
		{
			this.wakeFromSleep();
		} else if(keyCode != 28 && keyCode != 156)
		{
			super.keyTyped(typedChar, keyCode);
		} else
		{
			String s = this.inputField.getText().trim();
			
			if(!s.isEmpty())
				this.sendChatMessage(s);
			
			this.inputField.setText("");
			this.mc.ingameGUI.getChatGUI().resetScroll();
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 1)
			this.wakeFromSleep();
		else
			super.actionPerformed(button);
	}
	
	private void wakeFromSleep()
	{
		mc.player.connection.sendPacket(new CPacketEntityAction(mc.player, CPacketEntityAction.Action.STOP_SLEEPING));
		mc.player.connection.handleAnimation(new SPacketAnimation(mc.player, 2));
		mc.displayGuiScreen(null);
	}
}