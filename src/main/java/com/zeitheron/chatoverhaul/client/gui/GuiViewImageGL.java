package com.zeitheron.chatoverhaul.client.gui;

import java.io.IOException;

import com.zeitheron.chatoverhaul.utils.ImageCompressor;
import com.zeitheron.hammercore.client.utils.RenderUtil;

import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class GuiViewImageGL extends GuiScreen
{
	public int inTime, outTime = -1;
	public int image;
	public float aspectRatio;
	
	public float zoom = 1F, zcx, zcy;
	public int zoomProgress;
	public float prevZoom = 1F, pzcx, pzcy;
	
	public GuiViewImageGL(int glTex, float ar)
	{
		this.image = glTex;
		this.aspectRatio = ar;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		
		partialTicks = mc.getRenderPartialTicks();
		float scale = outTime != -1 ? (1 - Math.min(10, outTime + partialTicks) / 10F) : Math.min(10, inTime + partialTicks) / 10F;
		scale = (float) Math.sin(Math.toRadians(scale * 90));
		
		GlStateManager.enableAlpha();
		GlStateManager.enableBlend();
		GlStateManager.color(1, 1, 1, 1);
		
		GlStateManager.bindTexture(image);
		
		int w = width;
		int wh = ImageCompressor.getHeightByWidthNAR(w, aspectRatio);
		
		int h = height;
		int hw = ImageCompressor.getWidthByHeightNAR(h, aspectRatio);
		
		int wid, hei;
		
		if(w < hw)
		{
			wid = w;
			hei = wh;
		} else
		{
			wid = hw;
			hei = h;
		}
		
		GlStateManager.pushMatrix();
		GlStateManager.translate((width - wid * scale) / 2, (height - hei * scale) / 2, 0);
		GlStateManager.scale(wid / 256F * scale, hei / 256F * scale, 1);
		RenderUtil.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
		GlStateManager.popMatrix();
	}
	
	@Override
	public void updateScreen()
	{
		if(outTime != -1 && outTime < 10)
			++outTime;
		else if(outTime != -1)
			mc.displayGuiScreen(new GuiChat());
		
		if(inTime < 10)
			++inTime;
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(outTime == -1 && keyCode == 1)
			outTime = 0;
	}
}