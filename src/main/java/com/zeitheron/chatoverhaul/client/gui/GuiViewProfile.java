package com.zeitheron.chatoverhaul.client.gui;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Objects;

import org.lwjgl.input.Keyboard;

import com.mojang.authlib.GameProfile;
import com.zeitheron.chatoverhaul.client.utils.RenderPlayerBody;
import com.zeitheron.chatoverhaul.client.utils.RenderPlayerBody.PlayerRenderingInfo;
import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.chatoverhaul.net.vc.PacketStartVC;
import com.zeitheron.hammercore.ServerHCClientPlayerData;
import com.zeitheron.hammercore.client.HCClientOptions;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.net.HCNet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatAllowedCharacters;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.fml.relauncher.Side;

public class GuiViewProfile extends GuiScreen
{
	public final GameProfile user;
	public final PlayerRenderingInfo profileInfo = new PlayerRenderingInfo();
	
	DynGuiTex tex;
	String bio, bioURL;
	boolean bioSelected;
	
	public GuiViewProfile(GameProfile user)
	{
		this.user = user;
		profileInfo.profile = user;
	}
	
	protected void refreshBio()
	{
		HCClientOptions opts = Objects.equals(mc.player.getGameProfile().getId(), user.getId()) ? HCClientOptions.getOptions() : ServerHCClientPlayerData.DATAS.get(Side.CLIENT).opts(user.getId().toString());
		NBTTagCompound tag = opts.getCustomData();
		String bioN = tag.getString("ChatOverhaul_Bio");
		if(tag.hasKey("ChatOverhaul_Bio", NBT.TAG_STRING) && (bioURL == null || !Objects.equals(bioN, bioURL)))
			try
			{
				bio = URLDecoder.decode(bioURL = bioN, "UTF-8");
			} catch(UnsupportedEncodingException e)
			{
				bio = "";
				e.printStackTrace();
			}
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		GuiTexBakery b = new GuiTexBakery();
		b.body(0, 0, 200, 175).slot(8, 8, 44, 70);
		tex = b.bake();
		
		GuiCustomButton bt;
		addButton(bt = new GuiCustomButton(0, (width - 200) / 2 + 5, (height - 175) / 2 + 80, 50, 20, "CALL"));
		
		Keyboard.enableRepeatEvents(true);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		int i = (width - 200) / 2;
		int j = (height - 175) / 2;
		
		GuiButton bt = buttonList.get(0);
		
		bt.enabled = VoiceChatReceiver.VC == null && !user.getId().equals(Minecraft.getMinecraft().player.getGameProfile().getId()) && VoiceChatReceiver.OUTGOING == null;
		
		if(VoiceChatReceiver.VC != null && VoiceChatReceiver.VC.uid.equals(user.getId()))
		{
			bt.enabled = true;
			bt.displayString = TextFormatting.DARK_RED + "STOP VC";
		} else
			bt.displayString = "CALL";
		
		refreshBio();
		drawDefaultBackground();
		tex.render(i, j);
		
		fontRenderer.drawSplitString("@" + user.getName(), i + 54, j + 8, 138, 0);
		
		boolean hover = mouseX >= i + 54 && mouseY >= j + 10 + fontRenderer.FONT_HEIGHT && mouseX < i + 54 + fontRenderer.getStringWidth("Bio:") && mouseY < j + 10 + fontRenderer.FONT_HEIGHT * 2;
		TextFormatting tf = bioSelected ? TextFormatting.UNDERLINE : TextFormatting.RESET;
		
		fontRenderer.drawSplitString(tf + "Bio:" + TextFormatting.RESET + " " + bio + (!bioSelected && (bio == null || bio.isEmpty()) ? "..." : "") + (bioSelected && System.currentTimeMillis() % 1000L > 500L ? TextFormatting.RESET + "_" : ""), i + 54, j + 10 + fontRenderer.FONT_HEIGHT, 138, 0);
		
		GlStateManager.color(1F, 1F, 1F);
		RenderPlayerBody.drawEntityOnScreen(i + 30, j + 58 + 15, 30, (float) (i + 30) - mouseX, (float) (j + 58 + 15 - 50) - mouseY, profileInfo);
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		profileInfo.update();
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		
		int i = (width - 200) / 2;
		int j = (height - 175) / 2;
		
		boolean pbs = bioSelected;
		bioSelected = Objects.equals(mc.player.getGameProfile().getId(), user.getId()) && mouseX >= i + 54 && mouseY >= j + 10 + fontRenderer.FONT_HEIGHT && mouseX < i + 54 + fontRenderer.getStringWidth("Bio:") && mouseY < j + 10 + fontRenderer.FONT_HEIGHT * 2;
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		super.keyTyped(typedChar, keyCode);
		
		if(bioSelected && Objects.equals(mc.player.getGameProfile().getId(), user.getId()))
		{
			if(bio == null)
				bio = "";
			if(keyCode == Keyboard.KEY_BACK)
			{
				if(!bio.isEmpty())
					bio = bio.substring(0, bio.length() - 1);
				if(bio.endsWith(TextFormatting.RESET.toString().substring(0, 1)))
					bio = bio.substring(0, bio.length() - 1) + "&";
				
				HCClientOptions.getOptions().getCustomData().setString("ChatOverhaul_Bio", URLEncoder.encode(bio, "UTF-8"));
				HCClientOptions.getOptions().saveAndSendToServer();
			} else if(bio.length() < 116 && ChatAllowedCharacters.isAllowedCharacter(typedChar))
			{
				bio += typedChar;
				bioURL = null;
				
				for(TextFormatting tf : TextFormatting.values())
				{
					String cc = tf.toString().substring(1);
					bio = bio.replaceAll("&" + cc, tf.toString());
				}
				
				if(bio.endsWith(" "))
					bio = bio.trim() + " ";
				else
					bio = bio.trim();
				
				HCClientOptions.getOptions().getCustomData().setString("ChatOverhaul_Bio", URLEncoder.encode(bio, "UTF-8"));
				HCClientOptions.getOptions().saveAndSendToServer();
			}
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		super.actionPerformed(button);
		
		if(button.id == 0)
		{
			if(VoiceChatReceiver.VC != null && VoiceChatReceiver.VC.uid.equals(user.getId()))
			{
				VoiceChatReceiver.VC.closeChannel();
			} else
			{
				VoiceChatReceiver.OUTGOING = user.getId();
				HCNet.INSTANCE.sendToServer(new PacketStartVC().setTarget(user.getId()));
			}
		}
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
}