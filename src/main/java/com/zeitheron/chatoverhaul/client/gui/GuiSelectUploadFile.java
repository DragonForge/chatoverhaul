package com.zeitheron.chatoverhaul.client.gui;

import java.awt.Component;
import java.awt.HeadlessException;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.google.common.util.concurrent.AtomicDouble;
import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.client.ote.OTEUploadFile;
import com.zeitheron.chatoverhaul.client.ote.OnTopEffects;
import com.zeitheron.chatoverhaul.net.PacketSendChatLine;
import com.zeitheron.chatoverhaul.utils.DataFileHostAPI;
import com.zeitheron.chatoverhaul.utils.DataFileHostAPI.DataFileHostEntity;
import com.zeitheron.hammercore.lib.zlib.utils.Threading;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class GuiSelectUploadFile extends GuiScreen
{
	public GuiScreen parent;
	
	{
		if(parent == null)
			parent = Minecraft.getMinecraft().currentScreen;
		
		Threading.createAndStart(() ->
		{
			try
			{
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e)
			{
				e.printStackTrace();
			}
			
			JFileChooser jfc = new JFileChooser()
			{
				@Override
				protected JDialog createDialog(Component parent) throws HeadlessException
				{
					JDialog jd = super.createDialog(parent);
					jd.setAlwaysOnTop(true);
					jd.requestFocus();
					return jd;
				}
			};
			
			int result = jfc.showOpenDialog(null);
			
			Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().displayGuiScreen(new GuiChat()));
			
			if(result == JFileChooser.APPROVE_OPTION)
			{
				AtomicDouble progress = new AtomicDouble();
				OnTopEffects.effects.add(new OTEUploadFile(42, height - 28, 42, height - 28, progress, jfc.getSelectedFile()));
				DataFileHostEntity entity = DataFileHostAPI.post(jfc.getSelectedFile(), progress);
				NBTTagCompound data = new NBTTagCompound();
				data.setTag("Entity", entity.serialize());
				PacketSendChatLine.send(ChatOverhaul.HANDLES.getValue(new ResourceLocation("chatoverhaul", "file")), data);
			}
		});
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		// do not react to esc keys
	}
}