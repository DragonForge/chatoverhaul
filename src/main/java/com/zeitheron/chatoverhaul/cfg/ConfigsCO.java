package com.zeitheron.chatoverhaul.cfg;

import com.zeitheron.chatoverhaul.lib.streamlabs.StreamlabsClient;
import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyString;

import net.minecraftforge.common.config.Configuration;

@HCModConfigurations(modid = "chatoverhaul")
public class ConfigsCO implements IConfigReloadListener
{
	@ModConfigPropertyString(category = "streamlabs", name = "Socket API Token", defaultValue = "", allowedValues = {}, comment = "The Socket API Token. Can be found at https://streamlabs.com/dashboard/#/apisettings")
	public static String socketToken;
	
	@Override
	public void reloadCustom(Configuration cfgs)
	{
		StreamlabsClient.start();
	}
}