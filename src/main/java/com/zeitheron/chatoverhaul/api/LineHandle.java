package com.zeitheron.chatoverhaul.api;

import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistryEntry;

public abstract class LineHandle extends IForgeRegistryEntry.Impl<LineHandle>
{
	@SideOnly(Side.CLIENT)
	public abstract ChatLine toLine(NBTTagCompound nbt, int updateCounter);
	
	@SideOnly(Side.CLIENT)
	public ChatLine createLine(NBTTagCompound data)
	{
		int updCounter = Minecraft.getMinecraft().ingameGUI.getUpdateCounter();
		return toLine(data, updCounter);
	}
	
	@SideOnly(Side.CLIENT)
	public static <T extends ChatLine & IRenderableChatLine> T createAndLoadChatLine(NBTTagCompound nbt)
	{
		LineHandle cl = ChatOverhaul.HANDLES.getValue(new ResourceLocation(nbt.getString("RegistryKey")));
		if(cl != null)
		{
			IRenderableChatLine render = WorldUtil.cast(cl.createLine(nbt), IRenderableChatLine.class);
			render.setPrint(new ChatFingerprint(nbt.getCompoundTag("ChatFingerprint")));
			return (T) render;
		}
		return null;
	}
}