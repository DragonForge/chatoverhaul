package com.zeitheron.chatoverhaul.api;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.entity.player.PlayerEvent;

public class CustomMessageEvent extends PlayerEvent
{
	public final LineHandle handle;
	public final NBTTagCompound data;
	
	public CustomMessageEvent(EntityPlayer player, LineHandle instance, NBTTagCompound data)
	{
		super(player);
		this.handle = instance;
		this.data = data;
	}
}