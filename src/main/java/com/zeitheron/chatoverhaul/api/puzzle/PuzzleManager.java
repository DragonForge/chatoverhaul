package com.zeitheron.chatoverhaul.api.puzzle;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.utils.FinalFieldHelper;
import com.zeitheron.hammercore.utils.WorldUtil;

import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;

public class PuzzleManager
{
	public final PuzzleTrigger trigger;
	
	public Int2ObjectMap<Object> variables = new Int2ObjectArrayMap<>();
	public List<PuzzleMapping> elements = new ArrayList<>();
	
	public PuzzleManager(PuzzleTrigger trigger)
	{
		this.trigger = trigger;
		this.trigger.triggerListeners.add(this);
	}
	
	public <T> T getVariableAt(int pointer, Class<T> type)
	{
		return WorldUtil.cast(variables.get(pointer), type);
	}
	
	public void putVariableAt(int pointer, Object val)
	{
		if(pointer >= 0)
			variables.put(pointer, val);
	}
	
	public String run(Object... data)
	{
		// Store event variables
		for(int i = 0; i < data.length; ++i)
			variables.put(-data.length + i, data[i]);
		PuzzleContext ctx = new PuzzleContext();
		
		for(PuzzleMapping pm : elements)
		{
			PuzzleElement pe = pm.delegate;
			PuzzleInstance pi = pe.createInstance();
			ctx.add(pi);
		}
		
		for(int i = 0; i < ctx.size(); ++i)
		{
			PuzzleMapping pm = elements.get(i);
			PuzzleElement pe = pm.delegate;
			PuzzleInstance pi = ctx.get(i);
			
			Class<?>[] inp = pe.input();
			
			Object[] idata = new Object[inp.length];
			for(int j = 0; j < inp.length; ++j)
				idata[j] = getVariableAt(pm.inParamMap.getInt(j), inp[j]);
			
			pi.doInput(idata);
			
			Object[] odata = pi.execute();
			Class<?>[] out = pm.delegate.output();
			if(odata.length != out.length)
				return "Result of execution element " + pi + " is not equal with declaration's output size!";
			for(int j = 0; j < out.length; ++j)
				if(!out[j].isAssignableFrom(odata[j].getClass()))
					return "Result of execution element " + pi + " [" + j + "] is not equal with declaration's output parameter! (Expected " + out[j] + ", got " + odata[j] + ")";
				else
					putVariableAt(pm.outParamMap.get(j), odata[j]);
		}
		
		return null;
	}
	
	public void dispose()
	{
		trigger.triggerListeners.remove(this);
		
		try
		{
			variables.clear();
			elements.clear();
			
			FinalFieldHelper.setFinalField(PuzzleManager.class.getDeclaredField("variables"), this, null);
			FinalFieldHelper.setFinalField(PuzzleManager.class.getDeclaredField("elements"), this, null);
			FinalFieldHelper.setFinalField(PuzzleManager.class.getDeclaredField("trigger"), this, null);
		} catch(Throwable err)
		{
		}
	}
}