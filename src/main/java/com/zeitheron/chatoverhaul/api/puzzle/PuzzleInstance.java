package com.zeitheron.chatoverhaul.api.puzzle;

public abstract class PuzzleInstance
{
	public boolean testInput(Object... input)
	{
		return false;
	}
	
	public Class<?>[] getMissing(Object... input)
	{
		return new Class[0];
	}
	
	public void doInput(Object... input)
	{
	}
	
	public Object[] execute()
	{
		return new Object[0];
	}
	
	public abstract PuzzleInstance copy();
}