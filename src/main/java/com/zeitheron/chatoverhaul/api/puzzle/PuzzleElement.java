package com.zeitheron.chatoverhaul.api.puzzle;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.GameData;

public abstract class PuzzleElement
{
	public static final Map<ResourceLocation, PuzzleElement> elementRegistry = new HashMap<>();
	private ResourceLocation registryName = null;
	
	public abstract Class<?>[] input();
	
	public abstract Class<?>[] output();
	
	public abstract PuzzleInstance createInstance();
	
	public final PuzzleElement setRegistryName(String name)
	{
		if(getRegistryName() != null)
			throw new IllegalStateException("Attempted to set registry name with existing registry name! New: " + name + " Old: " + getRegistryName());
		this.registryName = GameData.checkPrefix(name);
		elementRegistry.put(registryName, this);
		return this;
	}
	
	// Helper functions
	public final PuzzleElement setRegistryName(ResourceLocation name)
	{
		return setRegistryName(name.toString());
	}
	
	public final PuzzleElement setRegistryName(String modID, String name)
	{
		return setRegistryName(modID + ":" + name);
	}
	
	@Nullable
	public final ResourceLocation getRegistryName()
	{
		return registryName != null ? registryName : null;
	}
}