package com.zeitheron.chatoverhaul.api.puzzle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.GameData;

public class PuzzleTrigger
{
	public static final Map<ResourceLocation, PuzzleTrigger> elementRegistry = new HashMap<>();
	final List<PuzzleManager> triggerListeners = new ArrayList<>();
	private ResourceLocation registryName = null;
	
	public String[] varNames;
	
	private final int rng = new Random().nextInt(Integer.MAX_VALUE / 2);
	
	public String getVarName(int sub)
	{
		return varNames != null && sub >= 0 && sub < varNames.length ? varNames[sub] : "var_" + Integer.toHexString(rng + sub * rng / 16);
	}
	
	public void trigger(Object... value)
	{
		for(PuzzleManager m : triggerListeners)
			m.run(value);
	}
	
	public final PuzzleTrigger setRegistryName(String name)
	{
		if(getRegistryName() != null)
			throw new IllegalStateException("Attempted to set registry name with existing registry name! New: " + name + " Old: " + getRegistryName());
		this.registryName = GameData.checkPrefix(name);
		elementRegistry.put(registryName, this);
		return this;
	}
	
	// Helper functions
	public final PuzzleTrigger setRegistryName(ResourceLocation name)
	{
		return setRegistryName(name.toString());
	}
	
	public final PuzzleTrigger setRegistryName(String modID, String name)
	{
		return setRegistryName(modID + ":" + name);
	}
	
	@Nullable
	public final ResourceLocation getRegistryName()
	{
		return registryName != null ? registryName : null;
	}
}