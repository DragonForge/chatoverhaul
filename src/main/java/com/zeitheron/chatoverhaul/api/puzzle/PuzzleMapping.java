package com.zeitheron.chatoverhaul.api.puzzle;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import net.minecraft.util.ResourceLocation;

public class PuzzleMapping
{
	public final ResourceLocation element;
	public final PuzzleElement delegate;
	public final IntList inParamMap = new IntArrayList();
	public final IntList outParamMap = new IntArrayList();
	
	public PuzzleMapping(ResourceLocation el)
	{
		this.element = el;
		delegate = PuzzleElement.elementRegistry.get(el);
	}
}