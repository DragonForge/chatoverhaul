package com.zeitheron.chatoverhaul.api;

import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Cancelable
@SideOnly(Side.CLIENT)
public class CustomMessageCreateEvent extends Event
{
	public final IRenderableChatLine chatLine;
	
	public CustomMessageCreateEvent(IRenderableChatLine chatLine)
	{
		this.chatLine = chatLine;
	}
}