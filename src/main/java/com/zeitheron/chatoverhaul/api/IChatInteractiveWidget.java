package com.zeitheron.chatoverhaul.api;

public interface IChatInteractiveWidget
{
	void mouseClick(int relX, int relY, int mouseButton);
}