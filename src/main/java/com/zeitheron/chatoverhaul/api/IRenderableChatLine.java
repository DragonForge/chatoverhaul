package com.zeitheron.chatoverhaul.api;

import java.util.List;

import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.common.util.INBTSerializable;

public interface IRenderableChatLine extends INBTSerializable<NBTTagCompound>
{
	int getWidth();
	
	int getHeight();
	
	default void update()
	{
	}
	
	void setPrint(ChatFingerprint print);
	
	ChatFingerprint getPrint();
	
	boolean render(int alpha255, int x, int y);
	
	default ITextComponent transformHoveredComponent(ITextComponent component)
	{
		return component;
	}
	
	default void handleMouseClick(int x, int y)
	{
	}
	
	default void addTooltip(int x, int y, List<String> tooltip)
	{
	}
	
	default void bindTexture(String sub)
	{
		ChatOverhaul.proxy.bindTexture(sub);
	}
}