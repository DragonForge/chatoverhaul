package com.zeitheron.chatoverhaul.net;

import java.util.List;
import java.util.Objects;

import com.zeitheron.chatoverhaul.api.IRenderableChatLine;
import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@MainThreaded
public class PacketDeleteChatLine implements IPacket
{
	static
	{
		IPacket.handle(PacketDeleteChatLine.class, PacketDeleteChatLine::new);
	}
	
	public ChatFingerprint print;
	
	public PacketDeleteChatLine withMessage(ChatFingerprint print)
	{
		this.print = print;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setTag("d", print.serializeNBT());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		print = new ChatFingerprint(nbt.getCompoundTag("d"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		List<ChatLine> cls = Minecraft.getMinecraft().ingameGUI.getChatGUI().chatLines;
		for(int i = 0; i < cls.size(); ++i)
		{
			ChatLine cl = cls.get(i);
			if(cl instanceof IRenderableChatLine && Objects.equals(((IRenderableChatLine) cl).getPrint(), print))
			{
				cls.remove(i);
				break;
			}
		}
		
		cls = Minecraft.getMinecraft().ingameGUI.getChatGUI().drawnChatLines;
		for(int i = 0; i < cls.size(); ++i)
		{
			ChatLine cl = cls.get(i);
			if(cl instanceof IRenderableChatLine && Objects.equals(((IRenderableChatLine) cl).getPrint(), print))
			{
				cls.remove(i);
				break;
			}
		}
		
		return null;
	}
	
	public static void remove(ChatFingerprint print)
	{
		PacketDeleteChatLine p = new PacketDeleteChatLine().withMessage(print);
		if(FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER)
			HCNet.INSTANCE.sendToAll(p);
		else
			p.executeOnClient(null);
	}
}