package com.zeitheron.chatoverhaul.net;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.util.UUID;

import com.zeitheron.hammercore.net.transport.ITransportAcceptor;
import com.zeitheron.hammercore.net.transport.TransportSessionBuilder;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class NetTransportSendScreenMP implements ITransportAcceptor
{
	@Override
	public void read(InputStream readable, int length)
	{
		try
		{
			byte[] data = new byte[length];
			readable.read(data);
			
			DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
			long least = dis.readLong();
			long most = dis.readLong();
			UUID target = new UUID(most, least);
			
			MinecraftServer serv = FMLCommonHandler.instance().getMinecraftServerInstance();
			if(serv != null)
			{
				EntityPlayerMP mp = serv.getPlayerList().getPlayerByUUID(target);
				if(mp != null)
					new TransportSessionBuilder().setAcceptor(NetTransportSendScreen.class).addData(data).build().sendTo(mp);
			}
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
	}
}