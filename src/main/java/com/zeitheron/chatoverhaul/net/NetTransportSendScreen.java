package com.zeitheron.chatoverhaul.net;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.imageio.ImageIO;

import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.chatoverhaul.lib.iflow.FlowImageGL;
import com.zeitheron.hammercore.client.utils.GLImageManager;
import com.zeitheron.hammercore.net.transport.ITransportAcceptor;

public class NetTransportSendScreen implements ITransportAcceptor
{
	@Override
	public void read(InputStream readable, int length)
	{
		if(VoiceChatReceiver.VC != null && VoiceChatReceiver.VC.screenRemote != null)
			try
			{
				// while(VoiceChatReceiver.VC != null &&
				// VoiceChatReceiver.VC.screenRemote == null)
				// Thread.sleep(100L);
				
				DataInputStream din = new DataInputStream(readable);
				din.readLong();
				din.readLong();
				
				int w = din.readInt();
				int h = din.readInt();
				
				GZIPInputStream gi = new GZIPInputStream(din);
				
				BufferedImage i = ImageIO.read(gi);
				
				BufferedImage target = VoiceChatReceiver.VC.screenRemote.image;
				Graphics2D g = target.createGraphics();
				g.drawImage(i, 0, 0, null);
				g.dispose();
				
				if(!FlowImageGL.clientThings.isEmpty())
					FlowImageGL.clientThings.clear();
				FlowImageGL.clientThings.add(() -> GLImageManager.loadTexture(VoiceChatReceiver.VC.screenRemote.image, VoiceChatReceiver.VC.screenRemote.glId, false));
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
	}
	
	public static byte[] convert(UUID target, BufferedImage regs)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		try
		{
			out.writeLong(target.getLeastSignificantBits());
			out.writeLong(target.getMostSignificantBits());
			
			BufferedImage i = new BufferedImage(regs.getWidth(), regs.getHeight(), BufferedImage.TYPE_USHORT_555_RGB);
			Graphics2D g = i.createGraphics();
			g.drawImage(regs, 0, 0, null);
			g.dispose();
			
			out.writeInt(i.getWidth());
			out.writeInt(i.getHeight());
			
			GZIPOutputStream go = new GZIPOutputStream(out);
			
			ImageIO.write(i, "jpeg", go);
			
			go.close();
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
		return baos.toByteArray();
	}
}