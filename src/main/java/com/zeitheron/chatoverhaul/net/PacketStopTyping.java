package com.zeitheron.chatoverhaul.net;

import java.util.UUID;

import com.zeitheron.chatoverhaul.client.utils.UserTypingData;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.nbt.NBTTagCompound;

@MainThreaded
public class PacketStopTyping implements IPacket
{
	public UUID target;
	
	public PacketStopTyping setTarget(UUID target)
	{
		this.target = target;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		if(target != null)
			nbt.setUniqueId("t", target);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		if(nbt.hasUniqueId("t"))
			target = nbt.getUniqueId("t");
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		HCNet.INSTANCE.sendToAll(new PacketStopTyping().setTarget(net.getSender().getGameProfile().getId()));
		return null;
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		int i = UserTypingData.TYPING_UUIDS.indexOf(target);
		if(i == -1)
			return null;
		UserTypingData.TYPING_TIMERS.set(i, 200);
		return null;
	}
}