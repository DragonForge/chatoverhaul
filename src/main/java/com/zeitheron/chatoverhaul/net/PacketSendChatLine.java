package com.zeitheron.chatoverhaul.net;

import java.util.concurrent.ThreadLocalRandom;

import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.api.CustomMessageCreateEvent;
import com.zeitheron.chatoverhaul.api.CustomMessageEvent;
import com.zeitheron.chatoverhaul.api.IRenderableChatLine;
import com.zeitheron.chatoverhaul.api.LineHandle;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineNew;
import com.zeitheron.chatoverhaul.utils.TextComponentHelper;
import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;
import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@MainThreaded
public class PacketSendChatLine implements IPacket
{
	static
	{
		IPacket.handle(PacketSendChatLine.class, PacketSendChatLine::new);
	}
	
	public NBTTagCompound data = new NBTTagCompound();
	
	public PacketSendChatLine withMessage(LineHandle handle, NBTTagCompound data)
	{
		this.data = data;
		this.data.setString("RegistryKey", handle.getRegistryName().toString());
		
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setTag("d", data);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		data = nbt.getCompoundTag("d");
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		data.setString("Sender", net.getSender().getDisplayNameString());
		data.setTag("ChatFingerprint", new ChatFingerprint().serializeNBT());
		
		// Call event before sending to allow pre-processing of the message
		LineHandle cl = ChatOverhaul.HANDLES.getValue(new ResourceLocation(data.getString("RegistryKey")));
		if(cl != null)
			MinecraftForge.EVENT_BUS.post(new CustomMessageEvent(net.getSender(), cl, data));
		
		HCNet.INSTANCE.sendToAll(this);
		return null;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		IRenderableChatLine cl = LineHandle.createAndLoadChatLine(data);
		if(MinecraftForge.EVENT_BUS.post(new CustomMessageCreateEvent(cl)))
			return null;
		ChatLine lc = (ChatLine) cl;
		
		String msg = lc.getChatComponent().getUnformattedText().replaceAll("\r", "\\\\r").replaceAll("\n", "\\\\n");
		GuiNewChat.LOGGER.info("[CHAT] [FP" + (cl.getPrint() != null ? Long.toString(cl.getPrint().l, Character.MAX_RADIX) : "--") + "] {}", msg);
		
		if(Minecraft.getMinecraft().player != null)
		{
			String name = Minecraft.getMinecraft().player.getGameProfile().getName();
			if(msg.contains("@" + name))
			{
				if(lc instanceof ChatLineNew)
				{
					ITextComponent txt = lc.getChatComponent();
					
					TwoTuple<ITextComponent, Integer> res = TextComponentHelper.highlight(txt, "@" + name);
					
					cl = new ChatLineNew(lc.getUpdatedCounter(), res.get1(), lc.getChatLineID());
					cl.setPrint(((ChatLineNew) lc).getPrint());
					
					Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.BLOCK_NOTE_BELL, 1.25F + ThreadLocalRandom.current().nextFloat() * .5F));
				}
			}
		}
		
		Minecraft.getMinecraft().ingameGUI.getChatGUI().chatLines.add(0, (ChatLine) cl);
		Minecraft.getMinecraft().ingameGUI.getChatGUI().drawnChatLines.add(0, (ChatLine) cl);
		return null;
	}
	
	public static void send(LineHandle line, NBTTagCompound nbt)
	{
		PacketSendChatLine p = new PacketSendChatLine().withMessage(line, nbt);
		if(FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER)
		{
			nbt.setTag("ChatFingerprint", new ChatFingerprint().serializeNBT());
			HCNet.INSTANCE.sendToAll(p);
		} else
			HCNet.INSTANCE.sendToServer(p);
	}
	
	public static void send(LineHandle line, NBTTagCompound nbt, EntityPlayerMP mp)
	{
		PacketSendChatLine p = new PacketSendChatLine().withMessage(line, nbt);
		if(FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER)
		{
			nbt.setString("Sender", mp.getDisplayNameString());
			nbt.setTag("ChatFingerprint", new ChatFingerprint().serializeNBT());
			MinecraftForge.EVENT_BUS.post(new CustomMessageEvent(mp, line, nbt));
			HCNet.INSTANCE.sendToAll(p);
		} else
			HCNet.INSTANCE.sendToServer(p);
	}
}