package com.zeitheron.chatoverhaul.net;

import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.management.UserListOpsEntry;

public class APacketRemCL implements IPacket
{
	public ChatFingerprint print;
	
	static
	{
		IPacket.handle(APacketRemCL.class, APacketRemCL::new);
	}
	
	public APacketRemCL setPrint(ChatFingerprint print)
	{
		this.print = print;
		
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.merge(print.serializeNBT());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		print = new ChatFingerprint(nbt);
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		EntityPlayerMP mp = net.getSender();
		
		if(mp != null && print != null)
		{
			UserListOpsEntry op = mp.server.getPlayerList().getOppedPlayers().getEntry(mp.getGameProfile());
			
			if(op != null ? op.getPermissionLevel() >= 4 : mp.server.getOpPermissionLevel() >= 4)
				PacketDeleteChatLine.remove(print);
		}
		
		return null;
	}
}