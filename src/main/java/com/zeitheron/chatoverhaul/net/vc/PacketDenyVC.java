package com.zeitheron.chatoverhaul.net.vc;

import java.util.UUID;

import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

public class PacketDenyVC implements IPacket
{
	public boolean mode;
	public UUID target;
	
	public PacketDenyVC setMode(boolean mode)
	{
		this.mode = mode;
		return this;
	}
	
	public PacketDenyVC setTarget(UUID target)
	{
		this.target = target;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setBoolean("m", mode);
		nbt.setUniqueId("t", target);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		mode = nbt.getBoolean("m");
		target = nbt.getUniqueId("t");
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.server != null)
		{
			EntityPlayerMP target = net.server.getPlayerList().getPlayerByUUID(this.target);
			if(target != null)
				HCNet.INSTANCE.sendTo(new PacketDenyVC().setMode(mode).setTarget(net.getSender().getGameProfile().getId()), target);
		}
		
		return null;
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		if(mode)
		{
			VoiceChatReceiver.INCOMING = null;
		} else
		{
			VoiceChatReceiver.OUTGOING = null;
		}
		return null;
	}
}