package com.zeitheron.chatoverhaul.net.vc;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.UUID;

import org.lwjgl.opengl.GL11;

import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.chatoverhaul.lib.iflow.FlowImageGL;
import com.zeitheron.hammercore.client.utils.GLImageManager;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

@MainThreaded
public class PacketSetupScreenShare implements IPacket
{
	public int width, height;
	public UUID target;
	
	public PacketSetupScreenShare setSize(int width, int height)
	{
		this.width = width;
		this.height = height;
		return this;
	}
	
	public PacketSetupScreenShare setTarget(UUID target)
	{
		this.target = target;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("t", target);
		nbt.setInteger("w", width);
		nbt.setInteger("h", height);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		target = nbt.getUniqueId("t");
		width = nbt.getInteger("w");
		height = nbt.getInteger("h");
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.server != null)
		{
			EntityPlayerMP target = net.server.getPlayerList().getPlayerByUUID(this.target);
			if(target != null)
				HCNet.INSTANCE.sendTo(new PacketSetupScreenShare().setSize(width, height).setTarget(net.getSender().getGameProfile().getId()), target);
		}
		
		return null;
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		VoiceChatReceiver vc = VoiceChatReceiver.VC;
		if(vc != null && vc.uid.equals(target))
		{
			int glid = GL11.glGenTextures();
			vc.screenRemote = new FlowImageGL(width, height, glid);
			
			BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = img.createGraphics();
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, width, height);
			g.dispose();
			GLImageManager.loadTexture(img, glid, false);
			
			synchronized(vc.sharingScreen)
			{
				vc.sharingScreen.notifyAll();
			}
		}
		
		return null;
	}
}