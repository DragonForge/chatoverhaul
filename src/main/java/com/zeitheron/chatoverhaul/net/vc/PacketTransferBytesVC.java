package com.zeitheron.chatoverhaul.net.vc;

import java.util.UUID;

import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketTransferBytesVC implements IPacket
{
	public UUID victim;
	public byte[] voice;
	
	public PacketTransferBytesVC setVictim(UUID victim)
	{
		this.victim = victim;
		return this;
	}
	
	public PacketTransferBytesVC setVoice(byte[] voice)
	{
		this.voice = voice;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("T", victim);
		nbt.setByteArray("V", voice);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		victim = nbt.getUniqueId("T");
		voice = nbt.getByteArray("V");
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.server != null)
		{
			EntityPlayerMP victim = net.server.getPlayerList().getPlayerByUUID(this.victim);
			if(victim != null && net.getSender() != null)
				HCNet.INSTANCE.sendTo(new PacketTransferBytesVC().setVictim(net.getSender().getGameProfile().getId()).setVoice(voice), victim);
			else
				return new PacketStopVC().setVc(this.victim);
		}
		
		return null;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		VoiceChatReceiver vc = VoiceChatReceiver.VC;
		if(vc != null && vc.uid.equals(victim))
			vc.writeData(voice);
		return null;
	}
}