package com.zeitheron.chatoverhaul.net.vc;

import java.util.Objects;
import java.util.UUID;

import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

@MainThreaded
public class PacketAcceptVC implements IPacket
{
	public UUID target;
	
	public PacketAcceptVC setTarget(UUID target)
	{
		this.target = target;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("t", target);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		target = nbt.getUniqueId("t");
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.server != null)
		{
			EntityPlayerMP target = net.server.getPlayerList().getPlayerByUUID(this.target);
			if(target != null)
			{
				HCNet.INSTANCE.sendTo(new PacketAcceptVC().setTarget(net.getSender().getGameProfile().getId()), target);
				HCNet.INSTANCE.sendTo(new PacketAcceptVC().setTarget(target.getGameProfile().getId()), net.getSender());
			}
		}
		
		return null;
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		if(Objects.equals(target, VoiceChatReceiver.INCOMING))
			VoiceChatReceiver.INCOMING = null;
		if(Objects.equals(target, VoiceChatReceiver.OUTGOING))
			VoiceChatReceiver.OUTGOING = null;
		if(VoiceChatReceiver.VC == null)
			VoiceChatReceiver.VC = new VoiceChatReceiver(target);
		return null;
	}
}