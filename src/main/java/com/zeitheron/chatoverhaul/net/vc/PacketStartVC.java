package com.zeitheron.chatoverhaul.net.vc;

import java.util.UUID;

import com.zeitheron.chatoverhaul.client.ote.OTEIncomingCall;
import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@MainThreaded
public class PacketStartVC implements IPacket
{
	public UUID target;
	
	public PacketStartVC setTarget(UUID target)
	{
		this.target = target;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("t", target);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		target = nbt.getUniqueId("t");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		if(VoiceChatReceiver.INCOMING == null && VoiceChatReceiver.VC == null)
		{
			VoiceChatReceiver.INCOMING = target;
			OTEIncomingCall.show();
		}
		else
			return new PacketDenyVC().setMode(false).setTarget(target);
		return null;
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.server != null)
		{
			EntityPlayerMP mp = net.server.getPlayerList().getPlayerByUUID(target);
			if(mp != null)
				HCNet.INSTANCE.sendTo(new PacketStartVC().setTarget(net.getSender().getGameProfile().getId()), mp);
		}
		return null;
	}
}