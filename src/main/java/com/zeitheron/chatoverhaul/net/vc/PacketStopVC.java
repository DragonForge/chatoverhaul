package com.zeitheron.chatoverhaul.net.vc;

import java.util.UUID;

import com.zeitheron.chatoverhaul.client.utils.SoundIO;
import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketStopVC implements IPacket
{
	public UUID vc;
	
	public PacketStopVC setVc(UUID vc)
	{
		this.vc = vc;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("v", vc);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		vc = nbt.getUniqueId("v");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		VoiceChatReceiver r = VoiceChatReceiver.VC;
		if(r != null && r.uid.equals(vc))
		{
			r.closeChannel();
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.BLOCK_NOTE_BELL, 0.125F));
		}
		return null;
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.server != null)
		{
			EntityPlayerMP target = net.server.getPlayerList().getPlayerByUUID(vc);
			if(target != null)
			{
				HCNet.INSTANCE.sendTo(new PacketStopVC().setVc(net.getSender().getGameProfile().getId()), target);
				HCNet.INSTANCE.sendTo(new PacketStopVC().setVc(target.getGameProfile().getId()), net.getSender());
			}
		}
		
		return null;
	}
}