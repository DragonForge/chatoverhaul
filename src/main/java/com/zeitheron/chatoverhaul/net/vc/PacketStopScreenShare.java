package com.zeitheron.chatoverhaul.net.vc;

import java.util.UUID;

import org.lwjgl.opengl.GL11;

import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.chatoverhaul.lib.iflow.FlowImageGL;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.MainThreaded;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

@MainThreaded
public class PacketStopScreenShare implements IPacket
{
	public UUID target;
	
	public PacketStopScreenShare setTarget(UUID target)
	{
		this.target = target;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("t", target);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		target = nbt.getUniqueId("t");
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.server != null)
		{
			EntityPlayerMP target = net.server.getPlayerList().getPlayerByUUID(this.target);
			if(target != null)
				HCNet.INSTANCE.sendTo(new PacketStopScreenShare().setTarget(net.getSender().getGameProfile().getId()), target);
		}
		
		return null;
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		VoiceChatReceiver vc = VoiceChatReceiver.VC;
		if(vc != null && vc.uid.equals(target) && vc.screenRemote != null)
		{
			GL11.glDeleteTextures(vc.screenRemote.glId);
			vc.screenRemote = null;
			FlowImageGL.clientThings.clear();
		}
		
		return null;
	}
}