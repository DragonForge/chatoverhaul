package com.zeitheron.chatoverhaul.net.vc;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.util.UUID;

import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.chatoverhaul.lib.iflow.FlowImageGL;
import com.zeitheron.hammercore.client.utils.GLImageManager;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagShort;
import net.minecraftforge.common.util.Constants.NBT;

public class PacketTransportVideo implements IPacket
{
	public UUID target;
	public short[] data;
	public int width, y;
	
	public PacketTransportVideo setTarget(UUID target)
	{
		this.target = target;
		return this;
	}
	
	public PacketTransportVideo setData(DataBufferUShort data, int width, int y, int height)
	{
		this.data = new short[height * width];
		this.width = width;
		this.y = y;
		System.arraycopy(data.getData(), width * y, this.data, 0, width * height);
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("t", target);
		nbt.setInteger("y", y);
		nbt.setInteger("w", width);
		
		NBTTagList l = new NBTTagList();
		for(short s : data)
			l.appendTag(new NBTTagShort(s));
		nbt.setTag("d", l);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		target = nbt.getUniqueId("t");
		y = nbt.getInteger("y");
		width = nbt.getInteger("w");
		
		NBTTagList l = nbt.getTagList("d", NBT.TAG_SHORT);
		data = new short[l.tagCount()];
		for(int i = 0; i < l.tagCount(); ++i)
			data[i] = ((NBTTagShort) l.get(i)).getShort();
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(net.server != null)
		{
			EntityPlayerMP target = net.server.getPlayerList().getPlayerByUUID(this.target);
			if(target != null)
			{
				PacketTransportVideo p = new PacketTransportVideo().setTarget(net.getSender().getGameProfile().getId());
				p.data = data;
				p.width = width;
				p.y = y;
				HCNet.INSTANCE.sendTo(p, target);
			}
		}
		
		return null;
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		if(VoiceChatReceiver.VC != null && VoiceChatReceiver.VC.screenRemote != null)
		{
			DataBufferUShort db = (DataBufferUShort) VoiceChatReceiver.VC.screenRemote.image.getRaster().getDataBuffer();
			System.arraycopy(data, 0, db.getData(), width * y, data.length);
			
			if(!FlowImageGL.clientThings.isEmpty())
				FlowImageGL.clientThings.clear();
			FlowImageGL.clientThings.add(() ->
			{
				try
				{
					GLImageManager.loadTexture(VoiceChatReceiver.VC.screenRemote.image, VoiceChatReceiver.VC.screenRemote.glId, false);
				} catch(NullPointerException npe)
				{
					// May be called when voice call channel is about to close.
				}
			});
		}
		return null;
	}
}