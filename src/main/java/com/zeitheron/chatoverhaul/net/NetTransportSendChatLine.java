package com.zeitheron.chatoverhaul.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.api.CustomMessageEvent;
import com.zeitheron.chatoverhaul.api.LineHandle;
import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.net.transport.ITransportAcceptor;
import com.zeitheron.hammercore.net.transport.NetTransport;
import com.zeitheron.hammercore.net.transport.TransportSession;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

public class NetTransportSendChatLine implements ITransportAcceptor
{
	public EntityPlayerMP mp;
	public Side side;
	
	@Override
	public void setInitialContext(Side side, PacketContext ctx)
	{
		mp = ctx.getSender();
		this.side = side;
	}
	
	@Override
	public void read(InputStream readable, int length)
	{
		PacketSendChatLine p = new PacketSendChatLine();
		try
		{
			p.data = CompressedStreamTools.readCompressed(readable);
			
			if(side == Side.CLIENT)
				p.executeOnClient(null);
			else
			{
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
				try
				{
					p.data.setString("Sender", mp.getDisplayNameString());
					p.data.setTag("ChatFingerprint", new ChatFingerprint().serializeNBT());
					
					// Call event before sending to allow pre-processing of the
					// message
					LineHandle cl = ChatOverhaul.HANDLES.getValue(new ResourceLocation(p.data.getString("RegistryKey")));
					if(cl != null)
						MinecraftForge.EVENT_BUS.post(new CustomMessageEvent(mp, cl, p.data));
					
					CompressedStreamTools.writeCompressed(p.data, baos);
					
					NetTransport.builder().setAcceptor(NetTransportSendChatLine.class).addData(baos.toByteArray()).build().sendToAll();
				} catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		} catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void send(LineHandle line, NBTTagCompound nbt)
	{
		PacketSendChatLine p = new PacketSendChatLine().withMessage(line, nbt);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try
		{
			if(FMLCommonHandler.instance().getEffectiveSide() != Side.SERVER)
				p.data.setString("Sender", ChatOverhaul.proxy.getLocalDisplayName());
			CompressedStreamTools.writeCompressed(p.data, baos);
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		
		TransportSession ts = NetTransport.builder().setAcceptor(NetTransportSendChatLine.class).addData(baos.toByteArray()).build();
		
		if(FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER)
			ts.sendToAll();
		else
			ts.sendToServer();
	}
}