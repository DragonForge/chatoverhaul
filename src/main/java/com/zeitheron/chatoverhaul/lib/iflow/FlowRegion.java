package com.zeitheron.chatoverhaul.lib.iflow;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class FlowRegion
{
	public final short[] data;
	public final int offset;
	
	public FlowRegion(int length, int offset)
	{
		this.data = new short[length];
		this.offset = offset;
	}
	
	public int getLength()
	{
		return this.data.length;
	}
	
	public void write(DataOutputStream out) throws IOException
	{
		out.writeShort(data.length);
		out.writeInt(offset);
		for(short i = 0; i < data.length; ++i)
			out.writeShort(data[i]);
	}
	
	public static FlowRegion read(DataInputStream in) throws IOException
	{
		short len = in.readShort();
		int off = in.readInt();
		FlowRegion reg = new FlowRegion(len, off);
		for(short i = 0; i < len; ++i)
			reg.data[i] = in.readShort();
		return reg;
	}
}