package com.zeitheron.chatoverhaul.lib.iflow;

import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glTexSubImage2D;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;

import com.zeitheron.hammercore.client.utils.GLImageManager;

import net.minecraft.client.Minecraft;

public class FlowImageGL extends FlowImage implements IFlowListener
{
	public static final List<Runnable> clientThings = new ArrayList<>();
	
	public final int glId;
	
	public FlowImageGL(int width, int height, int glId)
	{
		super(width, height);
		this.glId = glId;
		listeners.add(this);
	}
	
	@Override
	public void onRegion(FlowImage image, FlowRegion reg)
	{
		BufferedImage img = image.image;
		
		// Re-upload specified texture to VRAM
		if(!clientThings.isEmpty())
			clientThings.clear();
		clientThings.add(() -> GLImageManager.loadTexture(img, glId, false));
	}
	
	public static void subTexture(BufferedImage image, int glTex2d, int xOff, int yOff, int width, int height)
	{
		if(image == null)
			return;
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);
		for(int y = 0; y < image.getHeight(); y++)
		{
			for(int x = 0; x < image.getWidth(); x++)
			{
				int pixel = pixels[y * image.getWidth() + x];
				buffer.put((byte) ((pixel >> 16) & 0xFF));
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				buffer.put((byte) (pixel & 0xFF));
				buffer.put((byte) ((pixel >> 24) & 0xFF));
			}
		}
		buffer.flip();
		glBindTexture(GL_TEXTURE_2D, glTex2d);
		glTexSubImage2D(GL_TEXTURE_2D, 0, xOff, yOff, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	}
}