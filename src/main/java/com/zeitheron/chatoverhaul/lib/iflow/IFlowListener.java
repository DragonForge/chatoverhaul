package com.zeitheron.chatoverhaul.lib.iflow;

public interface IFlowListener
{
	void onRegion(FlowImage image, FlowRegion reg);
}