package com.zeitheron.chatoverhaul.lib.iflow;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.util.ArrayList;
import java.util.List;

public class FlowImage
{
	public final BufferedImage image;
	private final DataBufferUShort buffer;
	public boolean dirty;
	
	public List<IFlowListener> listeners = new ArrayList<>();
	
	public static FlowImage createScaled(int maxWidth, int maxHeight, int givenW, int givenH)
	{
		float aspectRatio = givenW / (float) givenH;
		
		int w = Math.min(maxWidth, givenW);
		int wh = ImageCompressor.getHeightByWidthNAR(maxWidth, aspectRatio);
		
		int h = Math.min(maxHeight, givenH);
		int hw = ImageCompressor.getWidthByHeightNAR(h, aspectRatio);
		
		BufferedImage target;
		
		if(w < hw)
		{
			h = w;
			w = wh;
		} else
			w = hw;
		
		return new FlowImage(w, h);
	}
	
	public FlowImage(int width, int height)
	{
		this.image = new BufferedImage(width, height, BufferedImage.TYPE_USHORT_555_RGB);
		this.buffer = null;
//		buffer = (DataBufferUShort) image.getData().getDataBuffer();
	}
	
	public void accept(FlowRegion reg)
	{
//		short[] data = buffer.getData();
//		System.arraycopy(reg.data, 0, data, reg.offset, reg.getLength());
		dirty = true;
		
		if(!listeners.isEmpty())
			for(IFlowListener f : listeners)
				f.onRegion(this, reg);
	}
	
	public List<FlowRegion> write(BufferedImage image, int chunkSize)
	{
		if(image.getWidth() != this.image.getWidth() || image.getHeight() != this.image.getHeight())
			return null;
		
		image = to565(image);
		
		List<FlowRegion> regs = new ArrayList<>();
		
		int pixSLM = 0;
		FlowRegion reg = null;
		
		for(int x = 0; x < image.getWidth(); ++x)
			for(int y = 0; y < image.getHeight(); ++y)
			{
				boolean changed = image.getRGB(x, y) != this.image.getRGB(x, y);
				
				if(changed)
				{
					this.image.setRGB(x, y, image.getRGB(x, y));
					
					int i = x + y * image.getWidth();
					if(reg == null)
					{
						pixSLM = 0;
						reg = new FlowRegion(chunkSize, i);
					}
					
//					reg.data[pixSLM] = buffer.getData()[i];
				}
				
				++pixSLM;
				
				if(reg != null && pixSLM >= chunkSize)
				{
					regs.add(reg);
					reg = null;
				}
			}
		
		return regs;
	}
	
	public static BufferedImage to565(BufferedImage src)
	{
		BufferedImage dst = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_USHORT_565_RGB);
		Graphics2D g = dst.createGraphics();
		g.drawImage(src, 0, 0, null);
		g.dispose();
		return dst;
	}
}