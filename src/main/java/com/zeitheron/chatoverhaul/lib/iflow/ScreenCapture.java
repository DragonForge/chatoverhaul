package com.zeitheron.chatoverhaul.lib.iflow;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import com.zeitheron.hammercore.lib.zlib.utils.Threading;

public class ScreenCapture
{
	public static Thread start(AtomicBoolean active, int pp, Consumer<BufferedImage> data)
	{
		return Threading.createAndStart("ChatOverhaulScreenCapture", () ->
		{
			try
			{
				Robot dolbaeb = new Robot();
				GraphicsDevice dev = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
				int width = dev.getDisplayMode().getWidth();
				int height = dev.getDisplayMode().getHeight();
				Rectangle fullScreen = new Rectangle(0, 0, width, height);
				FlowTimer timer = new FlowTimer(5F);
				while(active.get())
				{
					long start = System.currentTimeMillis();
					BufferedImage image = dolbaeb.createScreenCapture(fullScreen);
					image = ImageCompressor.scaleDownTo(853, 480, image, true);
					data.accept(image);
					timer.invoke();
				}
			} catch(Throwable err)
			{
				active.set(false);
			}
			Thread.currentThread().interrupt();
		});
	}
}