package com.zeitheron.chatoverhaul.lib.iflow;

public class FlowTimer
{
	public final float upsLim;
	public long lastUpdate;
	
	public FlowTimer(float ups)
	{
		upsLim = ups;
	}
	
	public void invoke()
	{
		if(lastUpdate == 0L)
			lastUpdate = System.currentTimeMillis() - (long) (1000L / upsLim);
		sleep((lastUpdate + (long) (1000L / upsLim)) - System.currentTimeMillis());
		lastUpdate = System.currentTimeMillis();
	}
	
	private static void sleep(long ms)
	{
		try
		{
			Thread.sleep(ms);
		} catch(Throwable err)
		{
		}
	}
}