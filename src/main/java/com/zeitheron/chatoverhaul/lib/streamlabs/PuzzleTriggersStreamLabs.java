package com.zeitheron.chatoverhaul.lib.streamlabs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.zeitheron.chatoverhaul.api.puzzle.PuzzleTrigger;
import com.zeitheron.hammercore.lib.zlib.json.JSONArray;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.utils.Joiner;

import net.minecraft.nbt.NBTTagCompound;

public class PuzzleTriggersStreamLabs
{
	public static final PuzzleTrigger DONATION = new PuzzleTrigger().setRegistryName("chatoverhaul", "donation");
	
	public static final PuzzleTrigger TWITCH_FOLLOW = new PuzzleTrigger().setRegistryName("chatoverhaul", "twitch.follow");
	public static final PuzzleTrigger TWITCH_SUBSCRIPTIONS = new PuzzleTrigger().setRegistryName("chatoverhaul", "twitch.subscriptions");
	public static final PuzzleTrigger TWITCH_HOST = new PuzzleTrigger().setRegistryName("chatoverhaul", "twitch.host");
	public static final PuzzleTrigger TWITCH_BITS = new PuzzleTrigger().setRegistryName("chatoverhaul", "twitch.bits");
	public static final PuzzleTrigger TWITCH_RAIDS = new PuzzleTrigger().setRegistryName("chatoverhaul", "twitch.raids");
	
	public static final PuzzleTrigger YOUTUBE_FOLLOW = new PuzzleTrigger().setRegistryName("chatoverhaul", "youtube.follow");
	public static final PuzzleTrigger YOUTUBE_SPONSOR = new PuzzleTrigger().setRegistryName("chatoverhaul", "youtube.sponsor");
	public static final PuzzleTrigger YOUTUBE_SUPERCHATS = new PuzzleTrigger().setRegistryName("chatoverhaul", "youtube.superchats");
	
	public static final PuzzleTrigger MIXER_FOLLOW = new PuzzleTrigger().setRegistryName("chatoverhaul", "mixer.follow");
	public static final PuzzleTrigger MIXER_SUBSCRIPTIONS = new PuzzleTrigger().setRegistryName("chatoverhaul", "mixer.subscriptions");
	public static final PuzzleTrigger MIXER_HOST = new PuzzleTrigger().setRegistryName("chatoverhaul", "mixer.host");
	
	static
	{
		DONATION.varNames = new String[] { "name", "amount", "format amount", "message" };
		
		TWITCH_FOLLOW.varNames = new String[] { "name" };
		TWITCH_SUBSCRIPTIONS.varNames = new String[] { "name", "months", "message", "subscription plan", "subscription plan name", "subscription type" };
		TWITCH_HOST.varNames = new String[] { "name", "viewers", "type" };
		TWITCH_BITS.varNames = new String[] { "name", "amount", "message" };
		TWITCH_RAIDS.varNames = new String[] { "name", "raiders" };
		
		YOUTUBE_FOLLOW.varNames = new String[] { "id", "name" };
		YOUTUBE_SPONSOR.varNames = new String[] { "id", "name", "channel URL", "months" };
		YOUTUBE_SUPERCHATS.varNames = new String[] { "id", "channel URL", "name", "message", "amount" };
		
		MIXER_FOLLOW.varNames = new String[] { "name" };
		MIXER_SUBSCRIPTIONS.varNames = new String[] { "name", "months" };
		MIXER_HOST.varNames = new String[] { "name", "viewers", "type" };
	}
	
	public static String omitURLs(String msg)
	{
		List<String> lines = new ArrayList<>(Arrays.asList(msg.split(" ")));
		for(int j = 0; j < lines.size(); ++j)
		{
			String l = lines.get(j);
			if(l.startsWith("http://") || l.startsWith("https://") || l.startsWith("ftp://") || l.startsWith("file://"))
				lines.set(j, "<URL>");
		}
		return Joiner.on(" ").join(lines);
	}
	
	public static void handleStreamLabsEvent(StreamlabsEvent e)
	{
		String f0r = e.body.optString("for", null);
		
		JSONArray o = e.getBody().optJSONArray("message");
		
		if(o == null)
			return;
		
		JSONObject jmsg = o.optJSONObject(0);
		
		if(jmsg == null)
			return;
		
		if(e.type.equalsIgnoreCase("donation"))
		{
			String name = jmsg.optString("name");
			String famt = jmsg.optString("formatted_amount");
			String msg = omitURLs(jmsg.optString("message"));
			Float amt = Float.parseFloat(jmsg.optString("amount"));
			DONATION.trigger(name, amt, famt, msg);
		} else if(f0r != null)
		{
			if(f0r.equalsIgnoreCase("twitch_account"))
			{
				switch(e.getType().toLowerCase())
				{
				case "subscription":
				{
					String name = jmsg.optString("name");
					Integer months = jmsg.optInt("months");
					String msg = omitURLs(jmsg.optString("message"));
					String subplan = jmsg.optString("sub_plan");
					String subplanname = jmsg.optString("sub_plan_name");
					String subtype = jmsg.optString("sub_type");
					TWITCH_SUBSCRIPTIONS.trigger(name, months, msg, subplan, subplanname, subtype);
				}
				break;
				case "follow":
				{
					String name = jmsg.optString("name");
					TWITCH_FOLLOW.trigger(name);
				}
				break;
				case "host":
				{
					String name = jmsg.optString("name");
					String viewers = jmsg.optString("viewers");
					String type = jmsg.optString("type");
					TWITCH_HOST.trigger(name, viewers, type);
				}
				break;
				case "bits":
				{
					String name = jmsg.optString("name");
					Integer amt = Integer.parseInt(jmsg.optString("amount"));
					String msg = omitURLs(jmsg.optString("message"));
					TWITCH_BITS.trigger(name, amt, msg);
				}
				break;
				case "raid":
				{
					String name = jmsg.optString("name");
					Integer raiders = jmsg.optInt("raiders");
					TWITCH_RAIDS.trigger(name, raiders);
				}
				break;
				default:
				break;
				}
			}
			
			if(f0r.equalsIgnoreCase("youtube_account"))
			{
				switch(e.getType().toLowerCase())
				{
				case "follow":
				{
					String id = jmsg.optString("id");
					String name = jmsg.optString("name");
					YOUTUBE_FOLLOW.trigger(id, name);
				}
				break;
				case "subscription":
				{
					String id = jmsg.optString("id");
					String name = jmsg.optString("name");
					String curl = jmsg.optString("channelUrl");
					Integer months = jmsg.optInt("months");
					YOUTUBE_SPONSOR.trigger(id, name, curl, months);
				}
				break;
				case "superchat":
				{
					String id = jmsg.optString("channelId");
					String curl = jmsg.optString("channelUrl");
					String name = jmsg.optString("name");
					String comment = omitURLs(jmsg.optString("comment"));
					String amount = jmsg.optString("displayString");
					YOUTUBE_SUPERCHATS.trigger(id, curl, name, comment, amount);
				}
				break;
				default:
				break;
				}
			}
			
			if(f0r.equalsIgnoreCase("mixer_account"))
			{
				switch(e.getType().toLowerCase())
				{
				case "follow":
				{
					MIXER_FOLLOW.trigger(jmsg.optString("name"));
				}
				break;
				case "subscription":
				{
					String name = jmsg.optString("name");
					Integer months = jmsg.optInt("months");
					MIXER_SUBSCRIPTIONS.trigger(name, months);
				}
				break;
				case "host":
				{
					String name = jmsg.optString("name");
					String viewers = jmsg.optString("viewers");
					String type = jmsg.optString("type");
					MIXER_HOST.trigger(name, viewers, type);
				}
				break;
				default:
				break;
				}
			}
		}
	}
}