package com.zeitheron.chatoverhaul.lib.streamlabs;

import com.zeitheron.hammercore.lib.zlib.json.JSONObject;

import net.minecraftforge.fml.common.eventhandler.Event;

public class StreamlabsEvent extends Event
{
	public final String type;
	public final JSONObject body;
	
	public StreamlabsEvent(String type, JSONObject body)
	{
		this.type = type;
		this.body = body;
	}
	
	public String getType()
	{
		return type;
	}
	
	public JSONObject getBody()
	{
		return body;
	}
}