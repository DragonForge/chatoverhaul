package com.zeitheron.chatoverhaul.lib.streamlabs;

import java.net.URISyntaxException;

import com.zeitheron.chatoverhaul.cfg.ConfigsCO;
import com.zeitheron.hammercore.lib.io.socket.client.IO;
import com.zeitheron.hammercore.lib.io.socket.client.Socket;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;

import net.minecraftforge.common.MinecraftForge;

public class StreamlabsClient
{
	public static boolean featureEnabled = false;
	private static Socket socket;
	
	public static void start()
	{
		try
		{
			if(socket != null)
			{
				featureEnabled = false;
				socket.disconnect();
				socket = null;
			}
			
			socket = IO.socket("https://sockets.streamlabs.com?token=" + ConfigsCO.socketToken);
			socket.on("event", data ->
			{
				Object main = data[0];
				
				JSONObject event = (JSONObject) main;
				String type = event.optString("type");
				
				StreamlabsEvent e = new StreamlabsEvent(type, event);
				PuzzleTriggersStreamLabs.handleStreamLabsEvent(e);
				MinecraftForge.EVENT_BUS.post(e);
			});
			socket.connect();
			featureEnabled = true;
		} catch(URISyntaxException e)
		{
			e.printStackTrace();
			featureEnabled = false;
		}
	}
}