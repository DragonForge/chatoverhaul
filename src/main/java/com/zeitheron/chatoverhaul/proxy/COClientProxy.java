package com.zeitheron.chatoverhaul.proxy;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.zeitheron.chatoverhaul.api.IRenderableChatLine;
import com.zeitheron.chatoverhaul.client.gui.GuiChatCO;
import com.zeitheron.chatoverhaul.client.gui.GuiOverhaulChat;
import com.zeitheron.chatoverhaul.client.gui.GuiSleepMPCO2;
import com.zeitheron.chatoverhaul.client.gui.GuiViewImage;
import com.zeitheron.chatoverhaul.client.gui.GuiViewProfile;
import com.zeitheron.chatoverhaul.client.ote.OnTopEffects;
import com.zeitheron.chatoverhaul.client.utils.SoundIO;
import com.zeitheron.chatoverhaul.client.utils.UserTypingData;
import com.zeitheron.chatoverhaul.client.utils.VoiceChatReceiver;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineImage;
import com.zeitheron.chatoverhaul.lhandles.lines.ChatLineVoice;
import com.zeitheron.chatoverhaul.lhandles.lines.streamlabs.ChatLineDonate;
import com.zeitheron.chatoverhaul.lib.iflow.FlowImageGL;
import com.zeitheron.chatoverhaul.lib.streamlabs.StreamlabsEvent;
import com.zeitheron.chatoverhaul.utils.ImageCompressor;
import com.zeitheron.hammercore.client.HCClientOptions;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.lib.zlib.json.JSONArray;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.utils.Joiner;
import com.zeitheron.hammercore.lib.zlib.utils.Threading;
import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSleepMP;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class COClientProxy extends COCommonProxy
{
	@Override
	public void preInit()
	{
		MinecraftForge.EVENT_BUS.register(new OnTopEffects());
		
		ClientCommandHandler.instance.registerCommand(new CommandBase()
		{
			@Override
			public String getName()
			{
				return "chatoverhaul_open_image";
			}
			
			@Override
			public String getUsage(ICommandSender sender)
			{
				return "";
			}
			
			@Override
			public int getRequiredPermissionLevel()
			{
				return 0;
			}
			
			@Override
			public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
			{
				if(args.length > 0)
				{
					String uuid = args[0];
					GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
					
					for(ChatLine cl : chat.chatLines)
						if(cl instanceof ChatLineImage)
						{
							ChatLineImage cli = (ChatLineImage) cl;
							if(cli.uuid.toString().equals(uuid))
							{
								Minecraft.getMinecraft().displayGuiScreen(new GuiViewImage(cli.getFullTex(), ImageCompressor.getAspectRatio(cli.full)));
								break;
							}
						}
				}
			}
		});
		
		ClientCommandHandler.instance.registerCommand(new CommandBase()
		{
			@Override
			public String getName()
			{
				return "chatoverhaul_play_voice";
			}
			
			@Override
			public String getUsage(ICommandSender sender)
			{
				return "";
			}
			
			@Override
			public int getRequiredPermissionLevel()
			{
				return 0;
			}
			
			@Override
			public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
			{
				if(args.length > 0)
				{
					String uuid = args[0];
					GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
					
					for(ChatLine cl : chat.chatLines)
						if(cl instanceof ChatLineVoice)
						{
							ChatLineVoice cli = (ChatLineVoice) cl;
							if(cli.uuid.toString().equals(uuid))
							{
								// Play voice
								if(cli.canPlay)
									Threading.createAndStart(() ->
									{
										if(!cli.canPlay)
											return;
										
										cli.canPlay = false;
										Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
										SoundIO.play(new ByteArrayInputStream(cli.entity), cli.entity.length, cli.progress);
										cli.canPlay = true;
									});
								break;
							}
						}
				}
			}
		});
		
		ClientCommandHandler.instance.registerCommand(new CommandBase()
		{
			@Override
			public String getName()
			{
				return "chatoverhaul_open_profile";
			}
			
			@Override
			public String getUsage(ICommandSender sender)
			{
				return "";
			}
			
			@Override
			public int getRequiredPermissionLevel()
			{
				return 0;
			}
			
			@Override
			public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
			{
				if(args.length > 0)
				{
					String name = args[0];
					
					NetworkPlayerInfo npi = Minecraft.getMinecraft().getConnection().getPlayerInfo(name);
					if(npi != null)
						Minecraft.getMinecraft().displayGuiScreen(new GuiViewProfile(npi.getGameProfile()));
				}
			}
		});
	}
	
	@Override
	public void bindTexture(String sub)
	{
		UtilsFX.bindTexture("chatoverhaul", "textures/" + sub + ".png");
	}
	
	@Override
	public String getLocalDisplayName()
	{
		return Minecraft.getMinecraft().player != null ? Minecraft.getMinecraft().player.getDisplayNameString() : Minecraft.getMinecraft().getSession().getUsername();
	}
	
	private boolean first = false;
	
	@SubscribeEvent
	public void clientTick(ClientTickEvent cte)
	{
		GuiIngame ign = Minecraft.getMinecraft().ingameGUI;
		
		if(!first)
		{
			first = true;
			
			Field persistantChatGUI = GuiIngame.class.getDeclaredFields()[6];
			try
			{
				FinalFieldHelper.setFinalField(persistantChatGUI, ign, new GuiOverhaulChat());
			} catch(ReflectiveOperationException e)
			{
				e.printStackTrace();
			}
		}
		
		if(cte.phase == Phase.END)
		{
			if(VoiceChatReceiver.VC != null && Minecraft.getMinecraft().world == null)
				VoiceChatReceiver.VC.closeChannel();
			
			int runs = 0;
			while(!FlowImageGL.clientThings.isEmpty() && runs < 1)
			{
				Runnable t = FlowImageGL.clientThings.remove(0);
				if(t != null)
					t.run();
				++runs;
			}
			
			UserTypingData.update();
			FlowImageGL.clientThings.clear();
			
			EntityPlayer p = Minecraft.getMinecraft().player;
			if(p != null && VoiceChatReceiver.INCOMING != null)
			{
				// if(p.ticksExisted % 20 == 0)
				// Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.BLOCK_NOTE_BELL,
				// 1F + (float) Math.sin(Math.toRadians((p.ticksExisted % 200) /
				// 200F * 90F))));
			}
			
			for(ChatLine cl : ign.getChatGUI().drawnChatLines)
				if(cl instanceof IRenderableChatLine)
				{
					IRenderableChatLine r = (IRenderableChatLine) cl;
					r.update();
				}
		}
	}
	
	@SubscribeEvent
	public void donation(StreamlabsEvent e)
	{
		if(e.type.equalsIgnoreCase("donation"))
		{
			JSONArray array = e.getBody().optJSONArray("message");
			for(int i = 0; i < array.length(); ++i)
			{
				JSONObject val = array.optJSONObject(i);
				
				NBTTagCompound data = new NBTTagCompound();
				data.setString("name", val.optString("name"));
				data.setString("formatted_amount", val.optString("formatted_amount"));
				
				String msg = val.optString("message");
				
				List<String> lines = new ArrayList<>(Arrays.asList(msg.split(" ")));
				for(int j = 0; j < lines.size(); ++j)
				{
					String l = lines.get(j);
					if(l.startsWith("http://") || l.startsWith("https://") || l.startsWith("ftp://") || l.startsWith("file://"))
						lines.set(j, "<URL>");
				}
				
				data.setString("message", Joiner.on(" ").join(lines));
				ChatLine cl = new ChatLineDonate(Minecraft.getMinecraft().ingameGUI.updateCounter + (int) ((data.getString("message").length() + data.getString("name").length() * 1.5) * 10), 0, data);
				Minecraft.getMinecraft().ingameGUI.getChatGUI().chatLines.add(0, cl);
				Minecraft.getMinecraft().ingameGUI.getChatGUI().drawnChatLines.add(0, cl);
			}
		}
	}
	
	@SubscribeEvent
	public void openGui(GuiOpenEvent e)
	{
		if(e.getGui() instanceof GuiSleepMP && !(e.getGui() instanceof GuiSleepMPCO2))
		{
			GuiSleepMPCO2 nev = new GuiSleepMPCO2();
			try
			{
				NBTTagCompound nbt = HCClientOptions.getOptions().getCustomData();
				if(nbt.hasKey("ChatOverhaulLastText", NBT.TAG_STRING))
					nev.queueSetText = URLDecoder.decode(nbt.getString("ChatOverhaulLastText"), "UTF-8");
			} catch(UnsupportedEncodingException e1)
			{
				e1.printStackTrace();
			}
			e.setGui(nev);
			return;
		}
		
		if(e.getGui() instanceof GuiChat && !(e.getGui() instanceof GuiChatCO))
		{
			GuiChatCO nev = new GuiChatCO();
			try
			{
				NBTTagCompound nbt = HCClientOptions.getOptions().getCustomData();
				if(nbt.hasKey("ChatOverhaulLastText", NBT.TAG_STRING))
					nev.queueSetText = URLDecoder.decode(nbt.getString("ChatOverhaulLastText"), "UTF-8");
				
				Field f = GuiChat.class.getDeclaredFields()[5];
				f.setAccessible(true);
				String queue = (String) f.get(e.getGui());
				
				if(queue != null && queue.length() > 0)
					nev.queueSetText = queue;
			} catch(Throwable e1)
			{
				e1.printStackTrace();
			}
			e.setGui(nev);
		}
		
		GuiScreen pgui = Minecraft.getMinecraft().currentScreen;
		
		if(pgui instanceof GuiChat && e.getGui() != pgui)
		{
			GuiChat gc = (GuiChat) pgui;
			try
			{
				if(GuiScreen.isShiftKeyDown())
					e.setGui(pgui);
				
				HCClientOptions.getOptions().getCustomData().setString("ChatOverhaulLastText", URLEncoder.encode(gc.inputField.getText(), "UTF-8"));
				HCClientOptions.getOptions().save();
			} catch(Throwable e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	@SubscribeEvent
	public void renderChatOverlay(RenderGameOverlayEvent.Chat e)
	{
	}
	
	@SubscribeEvent
	public void renderChat(GuiScreenEvent.DrawScreenEvent.Post e)
	{
		if(e.getGui() instanceof GuiSleepMPCO2 && !Minecraft.getMinecraft().player.isPlayerSleeping())
			Minecraft.getMinecraft().displayGuiScreen(null);
	}
	
	@SubscribeEvent
	public void mouseClick(GuiScreenEvent.MouseInputEvent.Pre e)
	{
		
	}
}