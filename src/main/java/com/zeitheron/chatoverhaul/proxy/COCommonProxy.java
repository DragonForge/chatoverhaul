package com.zeitheron.chatoverhaul.proxy;

import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;

import com.zeitheron.chatoverhaul.ChatOverhaul;
import com.zeitheron.chatoverhaul.net.NetTransportSendChatLine;
import com.zeitheron.chatoverhaul.net.PacketSendChatLine;
import com.zeitheron.chatoverhaul.utils.ImageCompressor;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class COCommonProxy
{
	public void preInit()
	{
	}
	
	public String getLocalDisplayName()
	{
		return "Server";
	}
	
	public void bindTexture(String sub)
	{
	}
	
	@SubscribeEvent(priority = EventPriority.LOWEST, receiveCanceled = false)
	public void serverChat(ServerChatEvent e)
	{
		NBTTagCompound data = new NBTTagCompound();
		ITextComponent user = new TextComponentString(e.getUsername());
		
		Style st = user.getStyle();
		
		st.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chatoverhaul_open_profile " + e.getUsername()));
		st.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentTranslation("chatoverhaul.viewprofile")));
		
		data.setString("Text", ITextComponent.Serializer.componentToJson(new TextComponentString("<").appendSibling(user).appendSibling(new TextComponentString("> ")).appendSibling(net.minecraftforge.common.ForgeHooks.newChatWithLinks(e.getMessage()))));
		PacketSendChatLine.send(ChatOverhaul.HANDLES.getValue(new ResourceLocation("chatoverhaul", "text")), data);
		
		e.setCanceled(true);
	}
}