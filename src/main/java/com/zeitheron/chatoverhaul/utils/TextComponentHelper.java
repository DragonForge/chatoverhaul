package com.zeitheron.chatoverhaul.utils;

import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;

import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class TextComponentHelper
{
	public static TwoTuple<ITextComponent, Integer> highlight(ITextComponent txt, String occ)
	{
		if(txt instanceof TextComponentTranslation)
		{
			Style s = txt.getStyle().createDeepCopy();
			txt = new TextComponentString(txt.getFormattedText());
			txt.setStyle(s);
		}
		
		if(txt instanceof TextComponentString)
		{
			String root = txt.getUnformattedComponentText();
			
			int found = 0;
			int li = root.indexOf(occ);
			while(li >= 0)
			{
				++found;
				li = root.indexOf(occ, li + occ.length());
			}
			
			root = root.replaceAll(occ, TextFormatting.YELLOW + occ + TextFormatting.RESET);
			
			TextComponentString str = new TextComponentString(root);
			str.setStyle(txt.getStyle().createDeepCopy());
			
			for(ITextComponent child : txt.getSiblings())
			{
				TwoTuple<ITextComponent, Integer>  f = highlight(child, occ);
				str.appendSibling(f.get1());
				found += f.get2();
			}
			
			return new TwoTuple<>(str, found);
		}
		
		return new TwoTuple<>(txt, 0);
	}
}