package com.zeitheron.chatoverhaul.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class CachingInputStream extends InputStream implements AutoCloseable
{
	public final PipedInputStream source;
	public final PipedOutputStream sink;
	
	public CachingInputStream(int bufferSize)
	{
		source = new PipedInputStream(bufferSize);
		
		PipedOutputStream pos = null;
		try
		{
			pos = new PipedOutputStream(source);
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		sink = pos;
	}
	
	public void write(byte[] b)
	{
		write(b, 0, b.length);
	}
	
	public void write(byte[] b, int off, int len)
	{
		try
		{
			sink.write(b, off, len);
		} catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void write(byte b)
	{
		try
		{
			sink.write(b);
		} catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public int read(byte[] b)
	{
		return read(b, 0, b.length);
	}
	
	@Override
	public int read(byte[] b, int off, int len)
	{
		try
		{
			return source.read(b, off, len);
		} catch(IOException e)
		{
			return 0;
		}
	}
	
	@Override
	public int read()
	{
		try
		{
			return source.read();
		} catch(IOException e)
		{
			return -1;
		}
	}
	
	@Override
	public void close() throws IOException
	{
		sink.close();
		source.close();
	}
}