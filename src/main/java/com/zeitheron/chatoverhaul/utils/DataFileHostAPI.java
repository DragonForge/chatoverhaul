package com.zeitheron.chatoverhaul.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.common.util.concurrent.AtomicDouble;
import com.zeitheron.hammercore.lib.zlib.io.IOUtils;
import com.zeitheron.hammercore.lib.zlib.web.HttpRequest;

import net.minecraft.nbt.NBTTagCompound;

public class DataFileHostAPI
{
	public static final int MAX_FILE_SIZE = 525336576;
	
	public static DataFileHostEntity post(File file, AtomicDouble progress)
	{
		try
		{
			return post(new FileInputStream(file), file.length(), file.getName(), progress);
		} catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static DataFileHostEntity post(InputStream data, long length, String filename, AtomicDouble progress)
	{
		if(length > MAX_FILE_SIZE)
			return null;
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		String cookie = "";
		
		HttpGet get = new HttpGet("https://www.datafilehost.com");
		try
		{
			cookie = httpClient.execute(get).getFirstHeader("Set-Cookie").getValue();
		} catch(IOException e1)
		{
			e1.printStackTrace();
			return null; // Can't connect
		}
		
		cookie = "__utmc=265377818; __utmz=265377818.1542230223.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utma=265377818.381579752.1542230223.1542230223.1542233295.2; " + cookie.split(";")[0] + "; __utmt=1; __atuvc=23%7C46; __atuvs=5bec9cce8adadad100c; __utmb=265377818.13.10.1542233295";
		
		HttpPost uploadFile = new HttpPost("https://www.datafilehost.com/upload.php");
		
		char[] available = "cA4e53gcZQYpgSc5".toCharArray();
		String bound = "----WebKitFormBoundary-";
		for(int i = 0; i < available.length; ++i)
			bound += available[ThreadLocalRandom.current().nextInt(available.length)];
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		StringBuilder sb = new StringBuilder();
		sb.append("--" + bound + "\r\nContent-Disposition: form-data; name=\"upfile\"; filename=\"" + filename + "\"\r\nContent-Type: */*\r\n\r\n");
		
		try
		{
			baos.write(sb.toString().getBytes());
			try
			{
				long totRead = 0L;
				byte[] buf = new byte[IOUtils.heapLimit];
				int read = 0;
				while((read = data.read(buf)) > 0)
				{
					baos.write(buf, 0, read);
					totRead += read;
					if(progress != null)
						progress.set(totRead / (double) length);
				}
			} catch(Throwable buf)
			{
				// empty catch block
			}
			sb = new StringBuilder();
			sb.append("\r\n--" + bound + "--");
			baos.write(sb.toString().getBytes());
		} catch(IOException e1)
		{
			e1.printStackTrace();
		}
		
		EntityBuilder multipart = EntityBuilder.create();
		multipart.setBinary(baos.toByteArray());
		
		uploadFile.setEntity(multipart.build());
		
		uploadFile.setHeader("Origin", "https://www.datafilehost.com");
		uploadFile.setHeader("Upgrade-Insecure-Requests", "1");
		uploadFile.setHeader("Content-Type", "multipart/form-data; boundary=" + bound);
		uploadFile.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36");
		
		uploadFile.setHeader("Cookie", cookie);
		
		try
		{
			CloseableHttpResponse resp = httpClient.execute(uploadFile);
			HttpEntity entity = resp.getEntity();
			
			baos.reset();
			
			String str = HttpRequest.get("https://www.datafilehost.com/codes.php") //
			        .header("Referer", "https://www.datafilehost.com/") //
			        .acceptEncoding("gzip, deflate, br") //
			        .header("Accept-Language", "en-US;q=0.9,en;q=0.8,uk;q=0.7") //
			        .header("Upgrade-Insecure-Requests", "1") //
			        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8") //
			        .header("Cookie", cookie) //
			        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36") //
			        .body();
			
			int clip;
			if((clip = str.indexOf("clip1.setText( '")) != -1)
			{
				String url = str.substring(clip + 16, str.indexOf("' );", clip + 16));
				resp.close();
				progress.set(2);
				return new DataFileHostEntity(url, filename, length);
			}
			
			resp.close();
			
			return null;
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static class DataFileHostEntity
	{
		public final String url, filename;
		public final long size;
		
		public DataFileHostEntity(String url, String filename, long size)
		{
			this.url = url;
			this.filename = filename;
			this.size = size;
		}
		
		public static DataFileHostEntity deserialize(NBTTagCompound nbt)
		{
			return new DataFileHostEntity(nbt.getString("URL"), nbt.getString("Name"), nbt.getLong("Size"));
		}
		
		public NBTTagCompound serialize()
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setString("URL", url);
			nbt.setString("Name", filename);
			nbt.setLong("Size", size);
			return nbt;
		}
	}
}