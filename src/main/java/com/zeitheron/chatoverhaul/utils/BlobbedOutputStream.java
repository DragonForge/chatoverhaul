package com.zeitheron.chatoverhaul.utils;

import java.io.OutputStream;
import java.util.function.Consumer;

public class BlobbedOutputStream extends OutputStream
{
	final byte[] blob;
	int write;
	
	final Consumer<byte[]> blobAcceptor;
	
	public BlobbedOutputStream(int blobSize, Consumer<byte[]> blobAcceptor)
	{
		this.blob = new byte[blobSize];
		this.blobAcceptor = blobAcceptor;
	}
	
	@Override
	public void write(byte[] b)
	{
		write(b, 0, b.length);
	}
	
	@Override
	public void write(byte[] b, int off, int len)
	{
		if(b == null)
			throw new NullPointerException();
		else if((off < 0) || (off > b.length) || (len < 0) || ((off + len) > b.length) || ((off + len) < 0))
			throw new IndexOutOfBoundsException();
		else if(len == 0)
			return;
		for(int i = 0; i < len; i++)
			write(b[off + i]);
	}
	
	@Override
	public void write(int b)
	{
		blob[write] = (byte) b;
		++write;
		
		if(write >= blob.length)
		{
			blobAcceptor.accept(blob);
			write = 0;
		}
	}
}